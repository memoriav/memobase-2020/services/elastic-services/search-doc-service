## Search Doc Service

A service which accepts memobase JSON-LD records, record sets and institutions and transforms them into their
respective index format. 

This is a highly specific transformation with many extra tweaks specifically needed by the frontend search.

[Confluence Doku](https://ub-basel.atlassian.net/wiki/spaces/MEMOBASE/pages/602210371/Search+Index+Transformation)
