/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.helpers.JsonUtility
import ch.memobase.reporting.ReportStatus
import ch.memobase.schemas.search.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import java.io.File
import java.nio.charset.Charset

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestDocument {
    private val dataPath = "src/test/resources/data/documents"
    private fun readFile(fileName: String): String {
        return File("$dataPath/$fileName").readText(Charset.defaultCharset())
    }

    private val transformer = DocumentsSearchDocTransformer(
        TestUtilities.translationMappers, TestUtilities.mediaUrl, "step", "test"
    )

    @Test
    fun `test documents search doc transformer`() {
        val output =
            transformer.transform(
                "key", JsonUtility.parseJson("key", readFile("test_document.json"), "step").first.map,
            )

        val result = output.first as Document
        val report = output.second

        val jsonOutput = result.toJson()

        assertAll(
            "",
            {
                assertThat(
                    jsonOutput.replace(
                        TestUtilities.dateRegex, "2020"
                    )
                ).describedAs("test full json output integrity.")
                    .isEqualTo(readFile("output.json").replace(TestUtilities.dateRegex, "2020"))
            },
            {
                assertThat(result.title).describedAs("test main title").isEqualTo(
                    listOf(
                        LanguageContainer(
                            de = listOf("Title DE", "Title UN"),
                            fr = listOf("Title FR", "Title UN"),
                            it = listOf("Title IT", "Title UN")
                        )
                    )
                )
            },
            {
                assertThat(result.seriesTitle).describedAs("test seriesTitle title").isEqualTo(
                    listOf(
                        LanguageContainer(
                            de = listOf("seriesTitle DE", "seriesTitle UN"),
                            fr = listOf("seriesTitle FR", "seriesTitle UN"),
                            it = listOf("seriesTitle IT", "seriesTitle UN")
                        )
                    )
                )
            },
            {
                assertThat(result.broadcastTitle).describedAs("test broadcastTitle title").isEqualTo(
                    listOf(
                        LanguageContainer(
                            de = listOf("broadcastTitle DE", "broadcastTitle UN"),
                            fr = listOf("broadcastTitle FR", "broadcastTitle UN"),
                            it = listOf("broadcastTitle IT", "broadcastTitle UN")
                        )
                    )
                )
            },
            {
                assertThat(result.type).describedAs("test type").isEqualTo(
                    FacetContainer(
                        name = LanguageContainer(
                            de = listOf("Fotografie"),
                            fr = listOf("Photographie"),
                            it = listOf("Fotografia"),
                        ), filter = "Foto", facet = emptyList()
                    ),
                )
            },
            {
                assertThat(result.sourceID).describedAs("test sourceId").isEqualTo("0002")
            },
            {
                assertThat(result.oldMemobaseId).describedAs("test oldMemobaseId").isEqualTo("TST-0002")
            },
            {
                assertThat(result.id).describedAs("test id").isEqualTo("tst-001-0002")
            },
            {
                assertThat(result.abstract).describedAs("test abstract field.").isEqualTo(
                    listOf(
                        LanguageContainer(
                            de = listOf("Abstract DE", "Abstract UN"),
                            fr = listOf("Abstract FR", "Abstract UN"),
                            it = listOf("Abstract IT", "Abstract UN")
                        )
                    )
                )
            },
            {
                assertThat(result.descriptiveNote).describedAs("test descriptiveNote field.").isEqualTo(
                    listOf(
                        LanguageContainer(
                            de = listOf("descriptiveNote DE", "descriptiveNote UN"),
                            fr = listOf("descriptiveNote FR", "descriptiveNote UN"),
                            it = listOf("descriptiveNote IT", "descriptiveNote UN")
                        )
                    )
                )
            },
            {
                assertThat(result.scopeAndContent).describedAs("test scopeAndContent field.").isEqualTo(
                    listOf(
                        LanguageContainer(
                            de = listOf("scopeAndContent DE", "scopeAndContent UN"),
                            fr = listOf("scopeAndContent FR", "scopeAndContent UN"),
                            it = listOf("scopeAndContent IT", "scopeAndContent UN")
                        )
                    )
                )
            },
            {
                assertThat(result.source).describedAs("test source field.").isEqualTo(
                    listOf(
                        LanguageContainer(
                            de = listOf("source DE", "source UN"),
                            fr = listOf("source FR", "source UN"),
                            it = listOf("source IT", "source UN")
                        )
                    )
                )
            },
            {
                assertThat(result.relatedMaterial).describedAs("test relatedMaterial field.").isEqualTo(
                    listOf(
                        LanguageContainer(
                            de = listOf("relatedMaterial DE", "relatedMaterial UN"),
                            fr = listOf("relatedMaterial FR", "relatedMaterial UN"),
                            it = listOf("relatedMaterial IT", "relatedMaterial UN")
                        )
                    )
                )
            },
            {
                assertThat(result.corporateBodyCreator).describedAs("test corporate body creator").isEqualTo(
                    listOf(
                        AgentWithRelationContainer(
                            name = LanguageContainer(
                                de = listOf("Creator Corporate Body DE", "Creator Corporate Body UN"),
                                fr = listOf("Creator Corporate Body FR", "Creator Corporate Body UN"),
                                it = listOf("Creator Corporate Body IT", "Creator Corporate Body UN")
                            ), relation = LanguageContainer(
                                de = listOf("Relation Name DE", "Relation Name UN"),
                                fr = listOf("Relation Name FR", "Relation Name UN"),
                                it = listOf("Relation Name IT", "Relation Name UN")
                            ), filter = null, facet = emptyList()
                        )
                    )
                )

            },
            {
                assertThat(result.corporateBodyContributor).describedAs("test corporate body contributor").isEqualTo(
                    listOf(
                        AgentWithRelationContainer(
                            name = LanguageContainer(
                                de = listOf("Contributor Corporate Body DE", "Contributor Corporate Body UN"),
                                fr = listOf("Contributor Corporate Body FR", "Contributor Corporate Body UN"),
                                it = listOf("Contributor Corporate Body IT", "Contributor Corporate Body UN")
                            ), relation = LanguageContainer(
                                de = listOf("Relation Name DE", "Relation Name UN"),
                                fr = listOf("Relation Name FR", "Relation Name UN"),
                                it = listOf("Relation Name IT", "Relation Name UN")
                            ), filter = null, facet = emptyList()
                        )
                    )
                )

            },
            {
                assertThat(result.agentCreator).describedAs("test agent creator").isEqualTo(
                    listOf(
                        AgentWithRelationContainer(
                            name = LanguageContainer(
                                de = listOf("Creator Agent DE", "Creator Agent UN"),
                                fr = listOf("Creator Agent FR", "Creator Agent UN"),
                                it = listOf("Creator Agent IT", "Creator Agent UN")
                            ), relation = LanguageContainer(
                                de = listOf("Relation Name DE", "Relation Name UN"),
                                fr = listOf("Relation Name FR", "Relation Name UN"),
                                it = listOf("Relation Name IT", "Relation Name UN")
                            ), filter = null, facet = emptyList()
                        )
                    )
                )

            },
            {
                assertThat(result.agentContributor).describedAs("test agent contributor").isEqualTo(
                    listOf(
                        AgentWithRelationContainer(
                            name = LanguageContainer(
                                de = listOf("Contributor Agent DE", "Contributor Agent UN"),
                                fr = listOf("Contributor Agent FR", "Contributor Agent UN"),
                                it = listOf("Contributor Agent IT", "Contributor Agent UN")
                            ), relation = LanguageContainer(
                                de = listOf("Relation Name DE", "Relation Name UN"),
                                fr = listOf("Relation Name FR", "Relation Name UN"),
                                it = listOf("Relation Name IT", "Relation Name UN")
                            ), filter = null, facet = emptyList()
                        )
                    )
                )
            },
            {
                assertThat(result.personCreator).describedAs("test person creator").isEqualTo(
                    listOf(
                        AgentWithRelationContainer(
                            name = LanguageContainer(
                                de = listOf("Creator Person DE", "Creator Person UN"),
                                fr = listOf("Creator Person FR", "Creator Person UN"),
                                it = listOf("Creator Person IT", "Creator Person UN")
                            ),
                            relation = LanguageContainer(
                                de = listOf("Relation Name DE", "Relation Name UN"),
                                fr = listOf("Relation Name FR", "Relation Name UN"),
                                it = listOf("Relation Name IT", "Relation Name UN")
                            ),
                            filter = "Creator Person UN",
                            facet = listOf(
                                "0~C~",
                                "1~C~Creator Person UN~"
                            )
                        )
                    )
                )

            },
            {
                assertThat(result.personContributor).describedAs("test person contributor").isEqualTo(
                    listOf(
                        AgentWithRelationContainer(
                            name = LanguageContainer(
                                de = listOf("Contributor Person DE", "Contributor Person UN"),
                                fr = listOf("Contributor Person FR", "Contributor Person UN"),
                                it = listOf("Contributor Person IT", "Contributor Person UN")
                            ), relation = LanguageContainer(
                                de = listOf("Relation Name DE", "Relation Name UN"),
                                fr = listOf("Relation Name FR", "Relation Name UN"),
                                it = listOf("Relation Name IT", "Relation Name UN")
                            ), filter = "Contributor Person UN", facet = listOf("0~C~", "1~C~Contributor Person UN~")
                        )
                    )
                )

            },
            {
                assertThat(result.dateCreated.first()).describedAs("test date created field.")
                    .isEqualTo(DateSearchField("1933-07-19", emptyList(), emptyList()))
            },
            {
                assertThat(result.dateIssued.first()).describedAs("test date issued field.")
                    .isEqualTo(DateSearchField("1933-07-19", emptyList(), emptyList()))
            },
            {
                assertThat(result.temporal.first()).describedAs("test temporal field.")
                    .isEqualTo(DateSearchField("1933-07-19", emptyList(), emptyList()))
            },
            {
                assertThat(result.dateFacetField).describedAs("test date facet field.").isEqualTo(
                    listOf(
                        DateFacetField(
                            "1933-07-19", "1933-07-19", "created", LanguageContainer(
                                listOf("0~20. Jahrhundert~", "1~20. Jahrhundert~1931-1940~"),
                                listOf("0~20. siècle~", "1~20. siècle~1931-1940~"),
                                listOf("0~20. secolo~", "1~20. secolo~1931-1940~")
                            )
                        ), DateFacetField(
                            "1933-07-19", "1933-07-19", "issued", LanguageContainer(
                                listOf("0~20. Jahrhundert~", "1~20. Jahrhundert~1931-1940~"),
                                listOf("0~20. siècle~", "1~20. siècle~1931-1940~"),
                                listOf("0~20. secolo~", "1~20. secolo~1931-1940~")
                            )
                        )
                    )
                )
            },
            {
                assertThat(result.language).isEqualTo(
                    listOf(
                        EnrichedFacetContainer(
                            LanguageContainer(
                                de = listOf("Swiss German"),
                                fr = listOf("Swiss German"),
                                it = listOf("Swiss German")
                            ), LanguageContainer(
                                listOf("Schweizerdeutsch"),
                                listOf("suisse allemand"),
                                listOf("svizzero tedesco"),
                            ), "content"
                        ), EnrichedFacetContainer(
                            LanguageContainer(
                                de = listOf("Swiss German"),
                                fr = listOf("Swiss German"),
                                it = listOf("Swiss German")
                            ), LanguageContainer(
                                listOf("Dialekt"),
                                listOf("dialecte"),
                                listOf("dialetto"),
                            ), "content"
                        ), EnrichedFacetContainer(
                            LanguageContainer(
                                de = listOf("French"),
                                fr = listOf("French"),
                                it = listOf("French")
                            ), LanguageContainer(
                                listOf("Französisch"),
                                listOf("français"),
                                listOf("francese"),
                            ), "content"
                        )
                    )
                )
            },
            {
                assertThat(result.keywords).describedAs("test keywords").isEqualTo(
                    LanguageContainer(
                        de = listOf("Porträt DE", "Porträt UN"),
                        fr = listOf("Porträt FR", "Porträt UN"),
                        it = listOf("Porträt IT", "Porträt UN")
                    )
                )
            },
            {
                assertThat(result.genre).describedAs("test genre").isEqualTo(
                    listOf(
                        EnrichedFacetContainer(
                            LanguageContainer(
                                de = listOf("SOURCE GENRE UN"),
                                fr = listOf("SOURCE GENRE UN"),
                                it = listOf("SOURCE GENRE UN")
                            ), LanguageContainer(
                                de = listOf("Genre DE", "Genre UN"),
                                fr = listOf("Genre FR", "Genre UN"),
                                it = listOf("Genre IT", "Genre UN")
                            ), null
                        ), EnrichedFacetContainer(
                            LanguageContainer(
                                de = listOf("SOURCE GENRE UN 2"),
                                fr = listOf("SOURCE GENRE UN 2"),
                                it = listOf("SOURCE GENRE UN 2")
                            ), LanguageContainer(
                                de = listOf("Genre 2 DE", "Genre 2 UN"),
                                fr = listOf("Genre 2 FR", "Genre 2 UN"),
                                it = listOf("Genre 2 IT", "Genre 2 UN")
                            ), null
                        )
                    )
                )
            },
            {
                assertThat(result.format).describedAs("test format").isEqualTo(
                    listOf(
                        EnrichedFacetContainer(
                            LanguageContainer(
                                de = listOf("source format UN"),
                                fr = listOf("source format UN"),
                                it = listOf("source format UN")
                            ), LanguageContainer(
                                de = listOf("result format DE 1"),
                                fr = listOf("result format FR 1"),
                                it = listOf("result format IT 1")
                            ), null
                        ), EnrichedFacetContainer(
                            LanguageContainer(
                                de = listOf("source format UN"),
                                fr = listOf("source format UN"),
                                it = listOf("source format UN")
                            ), LanguageContainer(
                                de = listOf("result format DE 2"),
                                fr = listOf("result format FR 2"),
                                it = listOf("result format IT 2")
                            ), null
                        )
                    )
                )
            },
            {
                assertThat(result.callNumber).describedAs("test call number").isEqualTo(listOf("67713"))
            },
            {
                assertThat(result.colourPhysical).describedAs("test colour physical").isEqualTo(
                    listOf(
                        LanguageContainer(
                            de = listOf("sw"),
                            fr = listOf("sw"),
                            it = listOf("sw")
                        )
                    )
                )
            },
            {
                assertThat(result.rightsHolder).describedAs("test rights holder").isEqualTo(
                    listOf(
                        LanguageContainer(
                            de = listOf("rightHolder DE", "rightHolder UN"),
                            fr = listOf("rightHolder FR", "rightHolder UN"),
                            it = listOf("rightHolder IT", "rightHolder UN")
                        )
                    )
                )
            },
            {
                assertThat(result.conditionsOfUse).describedAs("test conditions of use").isEqualTo(
                    listOf(
                        LanguageContainer(
                            de = listOf("conditionsOfUseDE", "conditionsOfUseUN"),
                            fr = listOf("conditionsOfUseFR", "conditionsOfUseUN"),
                            it = listOf("conditionsOfUseIT", "conditionsOfUseUN")
                        )
                    )
                )
            },
            {
                assertThat(result.sameAs).describedAs("test same as").isEqualTo(
                    listOf("https://example.org/orginial/source")
                )
            },
            {
                assertThat(result.personsFacet).describedAs("test persons facet").isEqualTo(
                    SimpleFacetContainer(
                        facet = listOf(
                            "0~C~",
                            "1~C~Contributor Person UN~",
                            "1~C~Creator Person UN~",
                            "0~L~",
                            "1~L~Last Name, First Name~"
                        ), filter = listOf("Contributor Person UN", "Creator Person UN", "Last Name, First Name")
                    )
                )
            },
            {
                assertThat(result.personSubject).describedAs("test persons subject").isEqualTo(
                    listOf(
                        AgentWithRelationContainer(
                            name = LanguageContainer(
                                listOf("Person DE", "Person UN"),
                                listOf("Person FR", "Person UN"),
                                listOf("Person IT", "Person UN")
                            ),
                            relation = null,
                            filter = "Last Name, First Name",
                            facet = listOf("0~L~", "1~L~Last Name, First Name~")
                        )
                    )
                )
            },
            {
                assertThat(result.personProducer).describedAs("test persons producer").isEqualTo(
                    listOf(
                        AgentWithRelationContainer(
                            name = LanguageContainer(
                                listOf("Person DE", "Person UN"),
                                listOf("Person FR", "Person UN"),
                                listOf("Person IT", "Person UN")
                            ),
                            relation = null,
                            filter = "Last Name, First Name",
                            facet = listOf("0~L~", "1~L~Last Name, First Name~")
                        )
                    )
                )
            },
            {
                assertThat(result.personPublisher).describedAs("test persons publisher").isEqualTo(
                    listOf(
                        AgentWithRelationContainer(
                            name = LanguageContainer(
                                listOf("Person DE", "Person UN"),
                                listOf("Person FR", "Person UN"),
                                listOf("Person IT", "Person UN")
                            ),
                            relation = null,
                            filter = "Last Name, First Name",
                            facet = listOf("0~L~", "1~L~Last Name, First Name~")
                        )
                    )
                )
            },
            {
                assertThat(result.corporateBodySubject).describedAs("test corporateBody subject").isEqualTo(
                    listOf(
                        AgentWithRelationContainer(
                            name = LanguageContainer(
                                listOf("CorporateBody DE", "CorporateBody UN"),
                                listOf("CorporateBody FR", "CorporateBody UN"),
                                listOf("CorporateBody IT", "CorporateBody UN")
                            ), relation = null, filter = null, facet = listOf()
                        )
                    )
                )
            },
            {
                assertThat(result.corporateBodyProducer).describedAs("test corporateBody producer").isEqualTo(
                    listOf(
                        AgentWithRelationContainer(
                            name = LanguageContainer(
                                listOf("CorporateBody DE", "CorporateBody UN"),
                                listOf("CorporateBody FR", "CorporateBody UN"),
                                listOf("CorporateBody IT", "CorporateBody UN")
                            ), relation = null, filter = null, facet = listOf()
                        )
                    )
                )
            },
            {
                assertThat(result.corporateBodyPublisher).describedAs("test corporateBody publisher").isEqualTo(
                    listOf(
                        AgentWithRelationContainer(
                            name = LanguageContainer(
                                listOf("CorporateBody DE", "CorporateBody UN"),
                                listOf("CorporateBody FR", "CorporateBody UN"),
                                listOf("CorporateBody IT", "CorporateBody UN")
                            ), relation = null, filter = null, facet = listOf()
                        )
                    )
                )
            },
            {
                assertThat(result.agentSubject).describedAs("test agent subject").isEqualTo(
                    listOf(
                        AgentWithRelationContainer(
                            name = LanguageContainer(
                                listOf("Agent DE", "Agent UN"),
                                listOf("Agent FR", "Agent UN"),
                                listOf("Agent IT", "Agent UN")
                            ), relation = null, filter = null, facet = listOf()
                        )
                    )
                )
            },
            {
                assertThat(result.agentProducer).describedAs("test agent producer").isEqualTo(

                    listOf(
                        AgentWithRelationContainer(
                            name = LanguageContainer(
                                listOf("Agent DE", "Agent UN"),
                                listOf("Agent FR", "Agent UN"),
                                listOf("Agent IT", "Agent UN")
                            ), relation = null, filter = null, facet = listOf()
                        )
                    )
                )
            },
            {
                assertThat(result.agentPublisher).describedAs("test agent publisher").isEqualTo(

                    listOf(
                        AgentWithRelationContainer(
                            name = LanguageContainer(
                                listOf("Agent DE", "Agent UN"),
                                listOf("Agent FR", "Agent UN"),
                                listOf("Agent IT", "Agent UN")
                            ), relation = null, filter = null, facet = listOf()
                        )
                    )
                )
            },
            {
                assertThat(result.published).describedAs("test published").isTrue()
            },
            {
                assertThat(result.memoriavClaim).describedAs("test memoriavClaim").isTrue()
            },
            {
                assertThat(result.suggest).describedAs("test suggest").isEqualTo(
                    SuggestContainer(
                        title = listOf("Title DE", "Title UN", "Title FR", "Title IT"),
                        seriesTitle = listOf(
                            "seriesTitle DE", "seriesTitle UN", "seriesTitle FR", "seriesTitle IT"
                        ),
                        broadcastTitle = listOf(
                            "broadcastTitle DE", "broadcastTitle UN", "broadcastTitle FR", "broadcastTitle IT"
                        ),
                        keywords = listOf("Porträt DE", "Porträt UN", "Porträt FR", "Porträt IT")
                    )
                )
            },
            {
                assertThat(result.digital).describedAs("test digital metadata").isEqualTo(
                    EnrichedDigitalMetadata(
                        isDistributedOn = "image",
                        hasMimeType = "image/jpeg",
                        hasFormat = "fmt/43",
                        height = "846.0",
                        width = "640.0",
                        aspectRatio = "320/423",
                        orientation = "Undefined",
                        mediaResourceDescription = "media resource description",
                        componentColor = listOf(
                            "000000", "A9A9A9", "C0D9D9", "545454"
                        )
                    )
                )
            },
            {
                assertThat(result.access).describedAs("test access").isEqualTo(
                    listOf(
                        EnrichedFacetContainer(
                            displayLabel = LanguageContainer(
                                listOf("vor Ort"), listOf("Sur place"), listOf("Nel local")
                            ), name = LanguageContainer(
                                listOf("4~vor Ort"), listOf("4~Sur place"), listOf("4~Nel local")
                            ), type = "onsite"
                        ), EnrichedFacetContainer(
                            displayLabel = LanguageContainer(
                                listOf("Online"), listOf("En ligne"), listOf("Online")
                            ), name = LanguageContainer(
                                listOf("1~Online"), listOf("1~En ligne"), listOf("1~Online")
                            ), type = "public"
                        )
                    )
                )
            },
            {
                assertThat(result.accessDigital).describedAs("test access digital").isEqualTo(
                    listOf(
                        FacetContainer(
                            LanguageContainer(
                                listOf("Online"), listOf("En ligne"), listOf("Online")
                            ), "public"
                        )
                    )
                )
            },
            {
                assertThat(result.accessPhysical).describedAs("test access physical").isEqualTo(
                    listOf(
                        FacetContainer(
                            LanguageContainer(
                                listOf("vor Ort"), listOf("Sur place"), listOf("Nel local")
                            ), "onsite"
                        )
                    )
                )
            },
            {
                assertThat(result.placeRelated).describedAs("test related places").isEqualTo(
                    listOf(
                        FacetContainer(
                            LanguageContainer(
                                listOf("Spatial DE", "Spatial UN"),
                                listOf("Spatial FR", "Spatial UN"),
                                listOf("Spatial IT", "Spatial UN")
                            ), "Spatial DE", listOf(
                                "0~S~",
                                "1~S~Spatial DE~",
                                "1~S~Spatial FR~",
                                "1~S~Spatial IT~",
                                "1~S~Spatial UN~",
                            )
                        )
                    )
                )
            },
            {
                assertThat(result.placeCapture).describedAs("test captured place").isEqualTo(
                    listOf(
                        FacetContainer(
                            LanguageContainer(
                                listOf("PlaceOfCapture DE", "PlaceOfCapture UN"),
                                listOf("PlaceOfCapture FR", "PlaceOfCapture UN"),
                                listOf("PlaceOfCapture IT", "PlaceOfCapture UN")
                            ), "PlaceOfCapture DE", listOf(
                                "0~P~",
                                "1~P~PlaceOfCapture DE~",
                                "1~P~PlaceOfCapture FR~",
                                "1~P~PlaceOfCapture IT~",
                                "1~P~PlaceOfCapture UN~",
                            )
                        )
                    )

                )
            },
            {
                assertThat(result.placeFacet).describedAs("test place facet").isEqualTo(
                    SimpleFacetContainer(
                        facet = listOf(
                            "0~P~",
                            "1~P~PlaceOfCapture DE~",
                            "1~P~PlaceOfCapture FR~",
                            "1~P~PlaceOfCapture IT~",
                            "1~P~PlaceOfCapture UN~",
                            "0~S~",
                            "1~S~Spatial DE~",
                            "1~S~Spatial FR~",
                            "1~S~Spatial IT~",
                            "1~S~Spatial UN~",
                        ),
                        filter = listOf(
                            "PlaceOfCapture DE",
                            "PlaceOfCapture FR",
                            "PlaceOfCapture IT",
                            "PlaceOfCapture UN",
                            "Spatial DE",
                            "Spatial FR",
                            "Spatial IT",
                            "Spatial UN"
                        ),
                    )
                )
            },
            {
                assertThat(result.institution).describedAs("test institution").isEqualTo(
                    emptyList<FacetContainer>()
                )
            },
            {
                assertThat(result.recordSet).describedAs("test record set").isEqualTo(
                    FacetContainer(
                        name = LanguageContainer.EMPTY, filter = null
                    ),
                )
            },
            {
                assertThat(result.durationDigital).describedAs("test duration digital")
                    .isEqualTo(listOf("duration digital"))
            },
            {
                assertThat(result.colourDigital).describedAs("test colour digital").isEqualTo(listOf("TrueColor"))
            },
            {
                assertThat(result.locator).describedAs("test locator")
                    .isEqualTo("https://media.memobase.k8s.unibas.ch/memo/tst-001-0002-1")
            },
            {
                assertThat(result.mediaLocation).describedAs("test media location").isEqualTo("remote")
            },
            {
                assertThat(result.usageDigital).describedAs("test usage digital")
                    .isEqualTo(listOf("http://rightsstatements.org/vocab/CNE/1.0/"))
            },
            {
                assertThat(result.usageDigitalGroup).describedAs("test usage digital group").isEqualTo(
                    listOf(
                        EnrichedFacetContainer(
                            displayLabel = LanguageContainer(
                                de = listOf("Urheberrechtsschutz nicht bewertet"),
                                fr = listOf("Droit d'auteur non évalué"),
                                it = listOf("Copyright non esaminato")
                            ), name = LanguageContainer(
                                de = listOf("Unklar (Copyright nicht bekannt)"),
                                fr = listOf("Inconnu (Copyright inconnu)"),
                                it = listOf("Non chiaro (Copyright non conosciuto)")
                            ), type = "http://rightsstatements.org/vocab/CNE/1.0/"
                        )
                    )
                )
            },
            {
                assertThat(result.digitalObjectNote).describedAs("test digital object note").isEqualTo(
                    listOf(
                        LanguageContainer(
                            de = listOf("Digital Object Note DE", "Digital Object Note UN"),
                            fr = listOf("Digital Object Note FR", "Digital Object Note UN"),
                            it = listOf("Digital Object Note IT", "Digital Object Note UN")
                        )
                    )
                )
            },
            {
                assertThat(result.usageConditionsDigital).describedAs("test usage conditions digital").isEqualTo(
                    listOf(
                        LanguageContainer(
                            listOf("digital conditionsOfUse DE", "digital conditionsOfUse UN"),
                            listOf("digital conditionsOfUse FR", "digital conditionsOfUse UN"),
                            listOf("digital conditionsOfUse IT", "digital conditionsOfUse UN")
                        )
                    )
                )
            },
            {
                assertThat(result.durationPhysical).describedAs("test duration physical")
                    .isEqualTo(listOf("duration physical"))
            },
            {
                assertThat(result.usagePhysical).describedAs("test usage physical")
                    .isEqualTo(listOf("http://rightsstatements.org/vocab/CNE/1.0/"))
            },
            {
                assertThat(result.physicalCharacteristics).describedAs("test physical characteristics").isEqualTo(
                    listOf(
                        LanguageContainer(
                            listOf(
                                "Physical Characteristics 1 DE",
                                "Physical Characteristics 2 DE",
                                "Physical Characteristics 3 DE",
                                "Physical Characteristics 1 UN",
                                "Physical Characteristics 2 UN",
                                "Physical Characteristics 3 UN"
                            ),
                            listOf(
                                "Physical Characteristics 1 FR",
                                "Physical Characteristics 2 FR",
                                "Physical Characteristics 3 FR",
                                "Physical Characteristics 1 UN",
                                "Physical Characteristics 2 UN",
                                "Physical Characteristics 3 UN"
                            ),
                            listOf(
                                "Physical Characteristics 1 IT",
                                "Physical Characteristics 2 IT",
                                "Physical Characteristics 3 IT",
                                "Physical Characteristics 1 UN",
                                "Physical Characteristics 2 UN",
                                "Physical Characteristics 3 UN"
                            )
                        )
                    )
                )
            },
            {
                assertThat(result.usageConditionsPhysical).describedAs("test usage conditions physical").isEqualTo(
                    listOf(
                        LanguageContainer(
                            listOf("physical conditionsOfUse DE", "physical conditionsOfUse UN"),
                            listOf("physical conditionsOfUse FR", "physical conditionsOfUse UN"),
                            listOf("physical conditionsOfUse IT", "physical conditionsOfUse UN")
                        )
                    )
                )
            },
            {
                assertThat(result.physicalObjectNote).describedAs("test physical object note").isEqualTo(
                    listOf(
                        LanguageContainer(
                            listOf("Physical Object Note DE", "Physical Object Note UN"),
                            listOf("Physical Object Note FR", "Physical Object Note UN"),
                            listOf("Physical Object Note IT", "Physical Object Note UN"),
                        )
                    )
                )
            },
            {
                assertThat(result.accessInstitution).describedAs("test access institution").isEqualTo(
                    emptyList<FacetContainer>()
                )
            },
            {
                assertThat(result.originalInstitution).describedAs("test original institution").isEqualTo(
                    emptyList<FacetContainer>()
                )
            },
            {
                assertThat(result.masterInstitution).describedAs("test master institution").isEqualTo(
                    emptyList<FacetContainer>()
                )
            },
            {
                assertThat(report.status).isEqualTo(ReportStatus.success)
            },
            {
                assertThat(report.message).isEqualTo("")
            },
            {
                assertThat(report.step).isEqualTo("step")
            },
        )
    }

    @Test
    fun `test minimal example`() {
        val output = transformer.transform(
            "key", JsonUtility.parseJson("key", readFile("test_minimal_example.json"), "step").first.map
        )

        val result = output.first as Document
        val report = output.second
        assertAll({
            assertThat(result.id).describedAs("test id").isEqualTo("tst-001-0001")
        }, {
            assertThat(report.status).isEqualTo(ReportStatus.warning)
        }, {
            assertThat(report.message).isEqualTo(
                """No main title found on record key.
No series title found on record key.
No broadcast title found on record key.
Found no isPublished property on record key. Set to false.
No dates added for temporal on record key.
No dates added for created on record key.
No dates added for issued on record key."""
            )
        })
    }

    @Test
    fun `test documents search doc transformer physical characteristics`() {
        val output = transformer.transform(
            "key", JsonUtility.parseJson("key", readFile("test_physical_characteristics.json"), "step").first.map
        )

        val result = output.first as Document
        val report = output.second
        assertAll(
            {
                assertThat(result.id).describedAs("test id").isEqualTo("tst-001-0003")
            }, {
                assertThat(result.physicalCharacteristics).isEqualTo(
                    listOf(
                        LanguageContainer(
                            de = listOf("Physical Characteristics Common Case"),
                            fr = listOf("Physical Characteristics Common Case"),
                            it = listOf("Physical Characteristics Common Case")
                        )
                    )
                )
            }, {
                assertThat(report.status).isEqualTo(ReportStatus.warning)
            }, {
                assertThat(report.message).isEqualTo(
                    """No main title found on record key.
No series title found on record key.
No broadcast title found on record key.
Found no isPublished property on record key. Set to false.
No dates added for temporal on record key.
No dates added for created on record key.
No dates added for issued on record key."""
                )
            })
    }

    @Test
    fun `test no physical characteristics`() {
        val output = transformer.transform(
            "key", JsonUtility.parseJson("key", readFile("test_no_phyiscal_characteristics.json"), "step").first.map
        )

        val result = output.first as Document
        val report = output.second
        assertAll(
            {
                assertThat(result.id).describedAs("test id").isEqualTo("tst-001-0003")
            },
            {
                assertThat(result.physicalCharacteristics).isEqualTo(listOf<LanguageContainer>())
            },
            {
                assertThat(report.status).isEqualTo(ReportStatus.warning)
            },
            {
                assertThat(report.message).isEqualTo(
                    """No main title found on record key.
No series title found on record key.
No broadcast title found on record key.
Found no isPublished property on record key. Set to false.
No dates added for temporal on record key.
No dates added for created on record key.
No dates added for issued on record key."""
                )
            }
        )
    }


    @Test
    @Disabled
    fun `test throw no exception`() {
        val output = transformer.transform(
            "key", JsonUtility.parseJson("key", readFile("test.json"), "step").first.map
        )

        val result = output.first as Document
        val report = output.second
        assertAll(
            {
                assertThat(result.id).describedAs("test id").isEqualTo("tst-001-0003")
            },
            {
                assertThat(result.physicalCharacteristics).isEqualTo(listOf<LanguageContainer>())
            },
            {
                assertThat(report.status).isEqualTo(ReportStatus.warning)
            },
            {
                assertThat(report.message).isEqualTo(
                    """No main title found on record key.
No series title found on record key.
No broadcast title found on record key.
Found no isPublished property on record key. Set to false.
No dates added for temporal on record key.
No dates added for created on record key.
No dates added for issued on record key."""
                )
            }
        )
    }


    @Test
    fun `test input file meg-001`() {
        val output = transformer.transform(
            "key", JsonUtility.parseJson("key", readFile("test-meg-001.json"), "step").first.map
        )

        val result = output.first as Document
        val report = output.second
        assertAll(
            {
                assertThat(result.id).describedAs("test id").isEqualTo("Default")
            }, {
                assertThat(report.status).isEqualTo(ReportStatus.fatal)
            }, {
                assertThat(report.message).isEqualTo(
                    "Invalid Input Exception: Expected a string for the identifier! JsonArray(value=[224, Bande No. 11, Bd708-1/1])."
                )
            })
    }
}