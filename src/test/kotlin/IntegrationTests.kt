/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.TestUtilities.translationMappers
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.kafka.streams.TopologyTestDriver
import org.apache.kafka.streams.test.TestRecord
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import java.io.File
import java.nio.charset.Charset

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class IntegrationTests {
    private val dataPath = "src/test/resources/data/integration"
    private fun readFile(fileName: String): String {
        return File("$dataPath/$fileName").readText(Charset.defaultCharset())
    }



    @Test
    fun `test documents search doc pipeline`() {
        val settings = App.createSettings("kafkaTest1.yml")
        val testDriver = TopologyTestDriver(
            KafkaTopology(settings, translationMappers).build(),
            settings.kafkaStreamsSettings
        )
        val inputTopic = testDriver.createInputTopic(settings.inputTopic, StringSerializer(), StringSerializer())
        val outputTopic = testDriver.createOutputTopic(settings.outputTopic, StringDeserializer(), StringDeserializer())
        val reportingTopic =
            testDriver.createOutputTopic(settings.processReportTopic, StringDeserializer(), StringDeserializer())


        inputTopic.pipeInput("test-1-key", readFile("input_records_jsonld.json"))
        val key = outputTopic.readRecord().key

        assertAll(
            "",
            {
                assertThat(outputTopic.queueSize)
                    .describedAs("output topic has one message.")
                    .isEqualTo(0)
            },
            {
                assertThat(reportingTopic.queueSize)
                    .describedAs("reporting topic has one message.")
                    .isEqualTo(1)
            },
            {
                assertThat(key)
                    .isEqualTo("afa-001-0847")
            }
        )
    }

    @Test
    fun `test documents search doc pipeline with null message`() {
        val settings = App.createSettings("kafkaTest1.yml")
        val testDriver = TopologyTestDriver(
            KafkaTopology(settings, translationMappers).build(),
            settings.kafkaStreamsSettings
        )
        val inputTopic = testDriver.createInputTopic(settings.inputTopic, StringSerializer(), StringSerializer())
        val outputTopic = testDriver.createOutputTopic(settings.outputTopic, StringDeserializer(), StringDeserializer())
        val reportingTopic =
            testDriver.createOutputTopic(settings.processReportTopic, StringDeserializer(), StringDeserializer())


        inputTopic.pipeInput(TestRecord("key", null))
        assertAll(
            "",
            {
                assertThat(outputTopic.queueSize)
                    .describedAs("output topic has no message.")
                    .isEqualTo(0)
            },
            {
                assertThat(reportingTopic.queueSize)
                    .describedAs("reporting topic has no message.")
                    .isEqualTo(0)
            }
        )
    }
}