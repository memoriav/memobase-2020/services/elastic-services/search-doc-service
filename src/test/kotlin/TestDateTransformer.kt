/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.helpers.DateTransformer
import ch.memobase.schemas.search.DateFacetField
import ch.memobase.schemas.search.DateSearchField
import ch.memobase.schemas.search.LanguageContainer
import ch.memobase.rdf.DC
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestDateTransformer {
    @Test
    fun `test single date and add to facet`() {
        val transformer = DateTransformer()
        transformer.add(
            "test", mapOf(
                Pair("@type", "rico:SingleDate"),
                Pair("normalizedDateValue", "1899-10-10"),
                Pair("certainty", "Nur bedingt korrekt"),
                Pair("dateQualifier", "vor")
            ), DC.created.localName, true
        )

        assertAll("", {
            assertThat(transformer.dateFacetFields).isEqualTo(
                listOf(
                    DateFacetField(
                        "1899-10-10", "1899-10-10", "created", LanguageContainer(
                            de = listOf("0~19. Jahrhundert~", "1~19. Jahrhundert~1891-1900~"),
                            fr = listOf("0~19. siècle~", "1~19. siècle~1891-1900~"),
                            it = listOf("0~19. secolo~", "1~19. secolo~1891-1900~"),

                            )
                    )
                )
            )
        }, {
            assertThat(transformer.dateSearchFields["created"]).isEqualTo(
                listOf(
                    DateSearchField(
                        "1899-10-10", listOf("Nur bedingt korrekt"), listOf("vor")
                    )
                )
            )
        })
    }

    @Test
    fun `test single date without facet`() {
        val transformer = DateTransformer()
        transformer.add(
            "test", mapOf(
                Pair("@type", "rico:SingleDate"),
                Pair("normalizedDateValue", "1899-10-10"),
                Pair("certainty", "Nur bedingt korrekt"),
                Pair("dateQualifier", "vor")
            ), DC.created.localName, false
        )

        assertAll("", {
            assertThat(transformer.dateFacetFields).isEqualTo(
                emptyList<DateFacetField>()
            )
        }, {
            assertThat(transformer.dateSearchFields["created"]).isEqualTo(
                listOf(
                    DateSearchField(
                        "1899-10-10", listOf("Nur bedingt korrekt"), listOf("vor")
                    )
                )
            )
        })
    }


    @Test
    fun `test date range with facet`() {
        val transformer = DateTransformer()
        transformer.add(
            "test", mapOf(
                Pair("@type", "rico:DateRange"),
                Pair("normalizedDateValue", "1899/1902"),
                Pair("certainty", "Some Stuff about it"),
                Pair("dateQualifier", "irgendwo dazwischen")
            ), DC.created.localName, true
        )

        assertAll("", {
            assertThat(transformer.dateFacetFields).isEqualTo(
                listOf(
                    DateFacetField(
                        "1899/1902", "1899/1902", "created", LanguageContainer(
                            de = listOf("0~19. Jahrhundert~", "1~19. Jahrhundert~1891-1900~", "0~20. Jahrhundert~", "1~20. Jahrhundert~1901-1910~"),
                            fr = listOf("0~19. siècle~", "1~19. siècle~1891-1900~", "0~20. siècle~", "1~20. siècle~1901-1910~"),
                            it = listOf("0~19. secolo~", "1~19. secolo~1891-1900~", "0~20. secolo~", "1~20. secolo~1901-1910~"),

                            )
                    )
                )
            )
        }, {
            assertThat(transformer.dateSearchFields["created"]).isEqualTo(
                listOf(
                    DateSearchField(
                        "1899/1902", listOf("Some Stuff about it"), listOf("irgendwo dazwischen")
                    )
                )
            )
        })
    }

    @Test
    fun `test date range without facet`() {
        val transformer = DateTransformer()
        transformer.add(
            "test", mapOf(
                Pair("@type", "rico:DateRange"),
                Pair("normalizedDateValue", "1899/1902"),
                Pair("certainty", "Some Stuff about it"),
                Pair("dateQualifier", "irgendwo dazwischen")
            ), DC.created.localName, false
        )

        assertAll("", {
            assertThat(transformer.dateFacetFields).isEqualTo(
                emptyList<DateFacetField>()
            )
        }, {
            assertThat(transformer.dateSearchFields["created"]).isEqualTo(
                listOf(
                    DateSearchField(
                        "1899/1902", listOf("Some Stuff about it"), listOf("irgendwo dazwischen")
                    )
                )
            )
        })
    }
}
