/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.helpers.TranslationMappers

object TestUtilities {
    private const val institutionTypePath = "src/test/resources/configs/institution-type-labels.csv"
    private const val accessTermPath = "src/test/resources/configs/access-term-labels.csv"
    private const val documentTypePath = "src/test/resources/configs/document-type-labels.csv"
    private const val reuseStatementPath = "src/test/resources/configs/reuse-statement-labels.csv"
    private const val reuseStatementDisplayLabelsPath = "src/test/resources/configs/reuse-statement-displayLabels.csv"
    const val mediaUrl = "https://media.memobase.k8s.unibas.ch/memo/"
    val dateRegex = Regex("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2})")

    val translationMappers =
        TranslationMappers(
            documentTypePath,
            accessTermPath,
            reuseStatementPath,
            reuseStatementDisplayLabelsPath
        )
}