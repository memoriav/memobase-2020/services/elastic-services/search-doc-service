/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.schemas.jsonld.Record
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromStream
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Disabled
import java.io.File

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Disabled
class TestParseJsonLd {
    @OptIn(ExperimentalSerializationApi::class)
    @Test
    fun `test parse json-ld`() {
        val input = Json.decodeFromStream<Record>(File("src/test/resources/data/jsonld/test01.json").inputStream())

        assertAll(
            { assertThat(input).isNotNull}
        )
    }
}