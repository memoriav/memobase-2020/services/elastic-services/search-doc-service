/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.helpers.DateFacetBuilder
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestDateFacetBuilder {


    @Test
    fun `test single date facet german`() {
        val builder = DateFacetBuilder()
        val output = builder.buildFromNormalizedSingleDate("1900-10-11", "de")
        assertAll("", {
            assertThat(output).containsAll(listOf("0~20. Jahrhundert~", "1~20. Jahrhundert~1891-1900~"))
        }, {
            assertThat(builder.errorList).isEqualTo(listOf<String>())
        })
    }

    @Test
    fun `test single date facet french`() {
        val builder = DateFacetBuilder()
        val output = builder.buildFromNormalizedSingleDate("2022-10-11", "fr")
        assertAll("", {
            assertThat(output).containsAll(
                listOf("0~21. siècle~", "1~21. siècle~2021-2030~")
            )
        }, {
            assertThat(builder.errorList).isEqualTo(listOf<String>())
        })
    }

    @Test
    fun `test single date facet italian`() {
        val builder = DateFacetBuilder()
        val output = builder.buildFromNormalizedSingleDate("1899-10-11", "it")
        assertAll("", {
            assertThat(output).containsAll(
                listOf("0~19. secolo~", "1~19. secolo~1891-1900~")
            )
        }, {
            assertThat(builder.errorList).isEqualTo(listOf<String>())
        })
    }


    @Test
    fun `test date range facet`() {
        val builder = DateFacetBuilder()
        val output = builder.buildFromNormalizedDateRange("1721/1951", "de")
        assertAll("", {
            assertThat(output).isEqualTo(
                listOf(
                    "0~18. Jahrhundert~",
                    "1~18. Jahrhundert~1721-1730~",
                    "1~18. Jahrhundert~1731-1740~",
                    "1~18. Jahrhundert~1741-1750~",
                    "1~18. Jahrhundert~1751-1760~",
                    "1~18. Jahrhundert~1761-1770~",
                    "1~18. Jahrhundert~1771-1780~",
                    "1~18. Jahrhundert~1781-1790~",
                    "1~18. Jahrhundert~1791-1800~",
                    "0~19. Jahrhundert~",
                    "1~19. Jahrhundert~1801-1810~",
                    "1~19. Jahrhundert~1811-1820~",
                    "1~19. Jahrhundert~1821-1830~",
                    "1~19. Jahrhundert~1831-1840~",
                    "1~19. Jahrhundert~1841-1850~",
                    "1~19. Jahrhundert~1851-1860~",
                    "1~19. Jahrhundert~1861-1870~",
                    "1~19. Jahrhundert~1871-1880~",
                    "1~19. Jahrhundert~1881-1890~",
                    "1~19. Jahrhundert~1891-1900~",
                    "0~20. Jahrhundert~",
                    "1~20. Jahrhundert~1901-1910~",
                    "1~20. Jahrhundert~1911-1920~",
                    "1~20. Jahrhundert~1921-1930~",
                    "1~20. Jahrhundert~1931-1940~",
                    "1~20. Jahrhundert~1941-1950~",
                    "1~20. Jahrhundert~1951-1960~"
                )
            )
        }, {
            assertThat(builder.errorList).isEqualTo(listOf<String>())
        })
    }
}
