/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.helpers.PlaceTransformer
import ch.memobase.helpers.SharedSearchDocUtility
import ch.memobase.schemas.search.FacetContainer
import ch.memobase.schemas.search.LanguageContainer
import ch.memobase.schemas.search.SimpleFacetContainer
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestPlaceTransformer {

    @Test
    fun `test place transformer`() {
        val transformer = PlaceTransformer(SharedSearchDocUtility("test"))
        transformer.add("test", mapOf(
                Pair("name", "Basel"),
                Pair("name_de", "Basel"),
                Pair("name_fr", "Basel"),
                Pair("name_it", "Basel")
        ), Constants.RdaProperties.HAS_PLACE_OF_CAPTURE)

        val places = transformer.places
        val facetValues = transformer.getCombinedPlaceFacet()
        assertAll("", {
            assertThat(places[Constants.RdaProperties.HAS_PLACE_OF_CAPTURE]).isEqualTo(
                listOf(
                    FacetContainer(
                        LanguageContainer(
                            listOf("Basel"),
                            listOf("Basel"),
                            listOf("Basel")
                        ),
                        "Basel",
                        listOf("0~B~", "1~B~Basel~")
                    )
                )
            )
        },
            {
                assertThat(facetValues)
                    .isEqualTo(SimpleFacetContainer(listOf("0~B~", "1~B~Basel~"), listOf("Basel")))
            }

            )
    }
}