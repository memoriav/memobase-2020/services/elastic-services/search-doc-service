/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.helpers.AspectRatio
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestFractions {
    @Test
    fun `test 16-9 aspect ratios`() {

        val result = AspectRatio.asFraction("1280", "720")
        assertThat(result).isEqualTo("16/9")
    }

    @Test
    fun `test 21-9 aspect ratios`() {
        val result = AspectRatio.asFraction("2520", "1080")
        assertThat(result).isEqualTo("7/3")
    }

    @Test
    fun `test 4-3 aspect ratios`() {
        val result = AspectRatio.asFraction("1280", "960")
        assertThat(result).isEqualTo("4/3")
    }

    @Test
    fun `test 3-2 aspect ratios`() {

        val result = AspectRatio.asFraction("1260", "840")
        assertThat(result).isEqualTo("3/2")
    }
}