/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.helpers.AgentTransformer
import ch.memobase.helpers.SharedSearchDocUtility
import ch.memobase.schemas.search.AgentWithRelationContainer
import ch.memobase.schemas.search.LanguageContainer
import ch.memobase.schemas.search.SimpleFacetContainer
import ch.memobase.rdf.RICO
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestAgentTransformer {
    @Test
    fun `test agent transformer without creation relation`() {
        val transformer = AgentTransformer(SharedSearchDocUtility("test-record"))
        transformer.add(
            "test", RICO.hasPublisher.localName, mapOf(
                Pair("@type", "rico:Person"),
                Pair("name", "Albert Einstein"),
                Pair("firstName", "Albert"),
                Pair("lastName", "Einstein")
            )
        )
        val output = transformer.getPersonFacetContainer()

        assertAll("", {
            assertThat(output).isEqualTo(
                SimpleFacetContainer(
                    listOf("0~E~", "1~E~Einstein, Albert~"), listOf("Einstein, Albert")
                )
            )
        })
    }


    @Test
    fun `test agent transformer with creation relation`() {
        val transformer = AgentTransformer(SharedSearchDocUtility("test-record"))
        transformer.addCreationRelation(
            "test", mapOf(
                Pair("@type", "rico:CreationRelation"),
                Pair("type", "contributor"),
                Pair("name", "Author"),
                Pair("name_de", "Author"),
                Pair("name_fr", "Author"),
                Pair("name_it", "Author"),
                Pair(
                    "creationRelationHasTarget", mapOf(
                        Pair("@type", "rico:CorporateBody"),
                        Pair("name", "Universitätsbibliothek Basel"),
                        Pair("name_de", "Universitätsbibliothek Basel"),
                        Pair("name_fr", "Universitätsbibliothek Basel"),
                        Pair("name_it", "Universitätsbibliothek Basel")
                    )
                ),
            )
        )
        val output = transformer.agentsMap.corporateBodies.contributor

        assertAll("", {
            assertThat(output).isEqualTo(
                listOf(
                    AgentWithRelationContainer(
                        LanguageContainer(
                            listOf("Universitätsbibliothek Basel"),
                            listOf("Universitätsbibliothek Basel"),
                            listOf("Universitätsbibliothek Basel")
                        ), LanguageContainer(
                            listOf("Author"), listOf("Author"), listOf("Author")
                        ), null, emptyList()
                    )
                )
            )
        })
    }

    @Test
    fun `test agent transformer with creation relation for person`() {
        val transformer = AgentTransformer(SharedSearchDocUtility("test-record"))
        transformer.addCreationRelation(
            "test", mapOf(
                Pair("@type", "rico:CreationRelation"),
                Pair("type", "contributor"),
                Pair("name", "Author"),
                Pair("name_de", "Author"),
                Pair("name_fr", "Author"),
                Pair("name_it", "Author"),
                Pair(
                    "creationRelationHasTarget", mapOf(
                        Pair("@type", "rico:Person"),
                        Pair("firstName", "Albert"),
                        Pair("lastName", "Einstein"),
                        Pair("name", "Albert Einstein")
                    )
                ),
            )
        )
        val output = transformer.agentsMap.persons.contributor

        assertAll("", {
            assertThat(output).isEqualTo(
                listOf(
                    AgentWithRelationContainer(
                        LanguageContainer(
                            listOf("Albert Einstein"),
                            listOf("Albert Einstein"),
                            listOf("Albert Einstein")
                        ), LanguageContainer(
                            listOf("Author"), listOf("Author"), listOf("Author")
                        ), "Einstein, Albert", listOf("0~E~", "1~E~Einstein, Albert~")
                    )
                )
            )
        })
    }
}
