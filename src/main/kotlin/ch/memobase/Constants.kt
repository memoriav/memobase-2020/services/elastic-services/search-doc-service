/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

object Constants {

    object SettingsProps {
        const val ACCESS_TERM_LABELS_PATH = "accessTermLabelsPath"
        const val REUSE_STATEMENT_LABELS_PATH = "reuseStatementLabelsPath"
        const val REUSE_STATEMENTS_DISPLAY_LABELS_PATH = "reuseStatementDisplayLabelsPath"
        const val DOCUMENT_TYPE_LABELS_PATH = "documentTypeLabelsPath"
        const val MEDIA_URL = "media.url"
        const val REPORTING_STEP_NAME = "reportingStepName"
        const val STEP_VERSION = "stepVersion"
    }

    /**
     * Facet separator value.
     *
     */
    const val FACET_SEPARATOR_VALUE = "~"

    /**
     * Facet level value.
     */
    const val FACET_LEVEL_0 = "0"

    /**
     * Facet level value.
     */
    const val FACET_LEVEL_1 = "1"

    /**
     * Property name for the RDF type property serialized in JSON-LD.
     */
    const val RDF_TYPE = "@type"

    /**
     * These names match the names defined in the frame of the record.
     */
    object FramedWikidataProperties {
        const val INSTANCE_OF = "instanceOf"
        const val COUNTRY = "country"
        const val CONTINENT = "continent"
        const val COORDINATES = "coordinateLocation"
        const val COORDINATES_OF_GEOGRAPHIC_CENTER = "coordinatesOfGeographicCenter"
    }

    /**
     * RDA properties are mapped to a human-readable name by the import process. Need to make sure to read
     * these properly.
     */
    object RdaProperties {
        /**
         * RDA property for the sponsoring agent of a resource written in camel case as text for JSON-LD.
         *
         * Value: hasSponsoringAgentOfResource
         *
         * [rda:P60451](http://rdaregistry.info/Elements/u/P60451)
         */
        const val HAS_SPONSORING_AGENT_OF_RESOURCE = "hasSponsoringAgentOfResource"

        /**
         * RDA property for the producer of a resource written in camel case as text for JSON-LD.
         *
         * Value: hasProducer
         *
         * [rda:P60441](http://www.rdaregistry.info/Elements/u/P60441)
         */
        const val HAS_PRODUCER = "hasProducer"

        /**
         * RDA property for the place of capture of a resource written in camel case as text for JSON-LD.
         *
         * Value: hasPlaceOfCapture
         *
         * [rda:P60556](http://rdaregistry.info/Elements/u/P60556)
         */
        const val HAS_PLACE_OF_CAPTURE = "hasPlaceOfCapture"
        /**
         * RDA property for the colour content of a resource written in camel case as text for JSON-LD.
         *
         * Value: hasColourContent
         *
         * [rda:P60558](http://rdaregistry.info/Elements/u/P60558)
         */
        const val HAS_COLOUR_CONTENT = "hasColourContent"
    }
}
