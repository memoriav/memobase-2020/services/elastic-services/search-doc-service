/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.Constants.RdaProperties
import ch.memobase.helpers.*
import ch.memobase.rdf.*
import ch.memobase.reporting.Report
import ch.memobase.reporting.ReportStatus
import ch.memobase.schemas.search.*
import ch.memobase.schemas.temp.DigitalObjectSearchDoc
import ch.memobase.schemas.temp.Instantiations
import ch.memobase.schemas.temp.PhysicalObjectSearchDoc
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.message.ObjectMessage

@Suppress("UNCHECKED_CAST")
class DocumentsSearchDocTransformer(
    private val translationMappers: TranslationMappers,
    private val mediaUrl: String,
    private val step: String,
    private val stepVersion: String,
) {
    private val log = LogManager.getLogger(this::class.java)

    fun transform(key: String, input: Map<String, Any?>): Pair<Document, Report> {
        return try {
            val results = generateSearchDoc(key, input)
            Pair(
                results.first, Report(
                    key,
                    if (results.second.isEmpty()) ReportStatus.success else ReportStatus.warning,
                    results.second.joinToString("\n"),
                    step,
                    stepVersion,
                )
            )
        } catch (ex: InvalidInputException) {
            log.error(ObjectMessage(ex))
            Pair(
                Document.DEFAULT, Report(
                    key,
                    ReportStatus.fatal,
                    "Invalid Input Exception: ${ex.localizedMessage}.",
                    step,
                    stepVersion,
                )
            )
        } catch (ex: NullPointerException) {
            log.error(ObjectMessage(ex))
            log.error(ObjectMessage(ex.stackTrace))
            Pair(
                Document.DEFAULT, Report(
                    key,
                    ReportStatus.fatal,
                    "Null Pointer Exception: ${ex.localizedMessage}.",
                    step,
                    stepVersion,
                )
            )
        } catch (ex: Exception) {
            log.error(ObjectMessage(ex))
            Pair(
                Document.DEFAULT, Report(
                    key,
                    ReportStatus.fatal,
                    "Unexpected Exception: ${ex.localizedMessage}.",
                    step,
                    stepVersion,
                )
            )
        }
    }

    private fun generateSearchDoc(key: String, input: Map<String, Any?>): Pair<Document, List<String>> {
        val functions = SharedSearchDocUtility("record")
        val errorList = mutableListOf<String>()
        val dateTransformer = DateTransformer()
        val placeTransformer = PlaceTransformer(functions)
        val agentTransformer = AgentTransformer(functions)

        try {
            processAgents(key, RICO.hasOrHadSubject.localName, input, agentTransformer)
        } catch (ex: InvalidInputException) {
            log.warn("Failed to process agent.")
            errorList.add("Invalid Input Exception: ${ex.localizedMessage}.")
        }
        try {
            processAgents(key, RICO.hasPublisher.localName, input, agentTransformer)
        } catch (ex: InvalidInputException) {
            log.warn("Failed to process agent.")
            errorList.add("Invalid Input Exception: ${ex.localizedMessage}.")
        }
        try {
            processAgents(key, RdaProperties.HAS_PRODUCER, input, agentTransformer)
        } catch (ex: InvalidInputException) {
            log.warn("Failed to process agent.")
            errorList.add("Invalid Input Exception: ${ex.localizedMessage}.")
        }
        processCreationRelations(key, input, agentTransformer)

        dateTransformer.add(key, input[DC.temporal.localName], DC.temporal.localName, false)
        dateTransformer.add(key, input[DC.created.localName], DC.created.localName, true)
        dateTransformer.add(key, input[DC.issued.localName], DC.issued.localName, true)

        placeTransformer.add(key, input[RdaProperties.HAS_PLACE_OF_CAPTURE], RdaProperties.HAS_PLACE_OF_CAPTURE)
        placeTransformer.add(key, input[DC.spatial.localName], DC.spatial.localName)

        val instantiations = processInstantiations(key, input[RICO.hasInstantiation.localName], functions, errorList)
        val physicalAccess = instantiations.physical?.access ?: emptyList()
        val digitalAccess = instantiations.digital?.access ?: emptyList()
        val access = physicalAccess + digitalAccess
        val accessEnriched = access.map {
            EnrichedFacetContainer(
                displayLabel = it.name.removePrefix(), name = it.name, type = it.filter
            )
        }
        val titles = functions.getTitle(key, input, RICO.Types.Title.main, "record")
        val seriesTitles = functions.getTitle(key, input, RICO.Types.Title.series, "record")
        val broadcastTitles = functions.getTitle(key, input, RICO.Types.Title.broadcast, "record")
        val keywords = functions.getZeroOneOrMoreMapsWithFilter(
            input[RICO.hasOrHadSubject.localName],
            SKOS.prefLabel.localName,
            Constants.RDF_TYPE,
            NS.namespaceToPrefix(NS.skos) + ":" + SKOS.Concept.localName
        ).reduceOrNull { acc, languageContainer -> acc.merge(languageContainer) } ?: LanguageContainer()
        val id = functions.getIdentifier(key, input, RICO.Types.Identifier.main, false)
        return Pair((Document(
            id = id,
            lastUpdatedDate = Date.now(),
            title = titles,
            seriesTitle = seriesTitles,
            broadcastTitle = broadcastTitles,
            type = input[RICO.type.localName].let {
                if (it == null) {
                    translationMappers.getDocumentType("Andere")
                } else {
                    translationMappers.getDocumentType(it as String)
                }
            },
            sourceID = functions.getIdentifier(key, input, RICO.Types.Identifier.original, false),
            oldMemobaseId = functions.getIdentifier(key, input, RICO.Types.Identifier.oldMemobase, true),
            sameAs = functions.getString(key, input, SCHEMA.sameAs.localName),
            abstract = functions.getTrilingualPropertyList(input, DC.abstract.localName),
            institution = emptyList(),
            recordSet = FacetContainer(
                name = LanguageContainer.EMPTY,
                filter = null
            ),
            descriptiveNote = functions.getTrilingualPropertyList(
                input, RICO.descriptiveNote.localName
            ),
            scopeAndContent = functions.getTrilingualPropertyList(
                input, RICO.scopeAndContent.localName
            ),
            relatedMaterial = functions.getTrilingualPropertyList(input, DC.relation.localName),
            source = functions.getTrilingualPropertyList(input, RICO.source.localName),

            temporal = dateTransformer.dateSearchFields.getOrDefault(DC.temporal.localName, emptyList()),
            dateCreated = dateTransformer.dateSearchFields.getOrDefault(DC.created.localName, emptyList()),
            dateIssued = dateTransformer.dateSearchFields.getOrDefault(DC.issued.localName, emptyList()),
            dateFacetField = dateTransformer.dateFacetFields,
            placeCapture = placeTransformer.places.getOrDefault(RdaProperties.HAS_PLACE_OF_CAPTURE, emptyList()),
            placeCaptureEnriched = placeTransformer.enrichedPlaces.getOrDefault(
                RdaProperties.HAS_PLACE_OF_CAPTURE,
                emptyList()
            ),
            placeRelated = placeTransformer.places.getOrDefault(DC.spatial.localName, emptyList()),
            placeRelatedEnriched = placeTransformer.enrichedPlaces.getOrDefault(DC.spatial.localName, emptyList()),
            placeFacet = placeTransformer.getCombinedPlaceFacet(),
            placeFacetEnriched = placeTransformer.getCombinedPlaceFacetEnriched(),
            enrichedPlacesCaptureMetadata = placeTransformer.enrichedPlaceMetadata[RdaProperties.HAS_PLACE_OF_CAPTURE]
                ?: emptyList(),
            enrichedPlacesRelatedMetadata = placeTransformer.enrichedPlaceMetadata[DC.spatial.localName] ?: emptyList(),
            rightsHolder = functions.getZeroOneOrMoreMapsWithFilter(
                input[RICO.isOrWasRegulatedBy.localName],
                RICO.name.localName,
                RICO.type.localName,
                RICO.Types.Rule.holder
            ),
            conditionsOfUse = functions.getTrilingualPropertyList(
                input, RICO.conditionsOfUse.localName
            ) + functions.getTrilingualPropertyList(input, RICO.conditionsOfAccess.localName),
            memoriavClaim = functions.getString(key, input, RdaProperties.HAS_SPONSORING_AGENT_OF_RESOURCE).any {
                it == NS.namespaceToPrefix(NS.mbcb) + ":" + MB.memoriavInstitutionURI.substringAfterLast("/")
            },

            format = instantiations.physical?.formats ?: emptyList(),
            language = getEnrichedValues(
                key,
                input[RICO.hasOrHadLanguage.localName],
                RICO.hasOrHadLanguage.localName,
                "record",
                RICO.name.localName,
                functions,
                errorList
            ),
            genre = getEnrichedValues(
                key,
                input[EBUCORE.hasGenre.localName],
                EBUCORE.hasGenre.localName,
                "record",
                SKOS.prefLabel.localName,
                functions,
                errorList
            ),

            keywords = keywords,

            personSubject = agentTransformer.agentsMap.persons.subject,
            personProducer = agentTransformer.agentsMap.persons.producer,
            personPublisher = agentTransformer.agentsMap.persons.publisher,
            personContributor = agentTransformer.agentsMap.persons.contributor,
            personCreator = agentTransformer.agentsMap.persons.creator,
            personsFacet = agentTransformer.getPersonFacetContainer(),

            corporateBodySubject = agentTransformer.agentsMap.corporateBodies.subject,
            corporateBodyProducer = agentTransformer.agentsMap.corporateBodies.producer,
            corporateBodyPublisher = agentTransformer.agentsMap.corporateBodies.publisher,
            corporateBodyContributor = agentTransformer.agentsMap.corporateBodies.contributor,
            corporateBodyCreator = agentTransformer.agentsMap.corporateBodies.creator,

            agentSubject = agentTransformer.agentsMap.agents.subject,
            agentProducer = agentTransformer.agentsMap.agents.producer,
            agentPublisher = agentTransformer.agentsMap.agents.publisher,
            agentContributor = agentTransformer.agentsMap.agents.contributor,
            agentCreator = agentTransformer.agentsMap.agents.creator,

            // DIGITAL & PHYSICAL
            access = accessEnriched,

            // DIGITAL
            accessDigital = digitalAccess.map { it.removePrefix() },
            durationDigital = instantiations.digital?.duration ?: emptyList(),
            colourDigital = instantiations.digital?.colour ?: emptyList(),
            digitalObjectNote = instantiations.digital?.descriptiveNote ?: emptyList(),
            locator = instantiations.digital?.locator,
            mediaLocation = instantiations.digital?.mediaLocation,
            usageConditionsDigital = instantiations.digital?.conditionsOfUse ?: emptyList(),
            usageDigital = instantiations.digital?.usageDigital ?: emptyList(),
            usageDigitalGroup = instantiations.digital?.usageDigital?.map { translationMappers.getReuseStatement(it) }
                ?: emptyList(),

            digital = instantiations.digital?.digital ?: EnrichedDigitalMetadata(),

            // PHYSICAL
            accessPhysical = physicalAccess.map { it.removePrefix() },
            durationPhysical = instantiations.physical?.duration ?: emptyList(),
            colourPhysical = instantiations.physical?.colour ?: emptyList(),
            physicalCharacteristics = instantiations.physical?.characteristics ?: emptyList(),
            physicalObjectNote = instantiations.physical?.descriptiveNote ?: emptyList(),
            usageConditionsPhysical = instantiations.physical?.conditionsOfUse ?: emptyList(),
            usagePhysical = instantiations.physical?.usage ?: emptyList(),
            callNumber = instantiations.physical?.callNumber ?: emptyList(),

            accessInstitution = emptyList(),
            originalInstitution = emptyList(),
            masterInstitution = emptyList(),

            published = functions.getBoolean(key, input, MB.isPublished.localName, "record"),
            suggest = SuggestContainer(title = titles.let {
                if (it.isEmpty()) {
                    emptyList()
                } else {
                    it.reduce { acc, languageContainer -> acc.merge(languageContainer) }.toSet()
                }
            }, seriesTitle = seriesTitles.let {
                if (it.isEmpty()) {
                    emptyList()
                } else {
                    it.reduce { acc, languageContainer -> acc.merge(languageContainer) }.toSet()
                }
            }, broadcastTitle = broadcastTitles.let {
                if (it.isEmpty()) {
                    emptyList()
                } else {
                    it.reduce { acc, languageContainer -> acc.merge(languageContainer) }.toSet()
                }
            }, keywords = keywords.toSet()
            )
        )), errorList + functions.errorList + dateTransformer.getErrors()
        )
    }

    private fun processInstantiations(
        key: String, input: Any?, functions: SharedSearchDocUtility, errorList: MutableList<String>
    ): Instantiations {
        var globalDigitalObject: Map<String, Any?>? = null
        var globalPhysicalObject: Map<String, Any?>? = null

        fun assignObject(item: Map<*, *>) {
            if (item.containsKey(RICO.type.localName)) {
                if (item[RICO.type.localName] == RICO.Types.Instantiation.digitalObject) {
                    globalDigitalObject = item as Map<String, Any?>
                } else if (item[RICO.type.localName] == RICO.Types.Instantiation.physicalObject) {
                    globalPhysicalObject = item as Map<String, Any?>
                }
            } else {
                log.error("Instantiation on record $key without a rico:type. This should not happen!")
                throw InvalidInputException("Instantiation on record $key without a rico:type. This should not happen!")
            }
        }

        when (input) {
            is List<*> -> {
                input.forEach {
                    when (it) {
                        is Map<*, *> -> assignObject(it)

                        else -> {
                            log.error("Instantiations on record $key have an unexpected structure.")
                            throw InvalidInputException("Instantiations on record $key have an unexpected structure.")
                        }
                    }

                }
            }

            is Map<*, *> -> assignObject(input)

            else -> {
                log.info("No instantiations for record $key found.")
                return Instantiations(null, null)
            }
        }

        val digitalObjectSearchDoc = globalDigitalObject.let { digitalObject ->
            if (digitalObject != null) {
                val identifier = functions.getIdentifier(key, digitalObject, RICO.Types.Identifier.main, false)
                val width = functions.getString(key, digitalObject, EBUCORE.width.localName).getOrElse(0) { "" }
                val height = functions.getString(key, digitalObject, EBUCORE.height.localName).getOrElse(0) { "" }
                DigitalObjectSearchDoc(
                    descriptiveNote =
                    functions.getTrilingualPropertyList(
                        digitalObject, RICO.descriptiveNote.localName

                    ),
                    locator = digitalObject[EBUCORE.locator.localName].let { loc ->
                        if (loc != null && loc is String) {
                            "${mediaUrl}${identifier}"
                        } else {
                            null
                        }
                    },
                    mediaLocation = functions.getString(key, digitalObject, MB.mediaLocation.localName).firstOrNull(),
                    conditionsOfUse =
                    functions.getTrilingualPropertyList(
                        digitalObject, RICO.conditionsOfUse.localName
                    ),
                    usageDigital = functions.getZeroOrMoreMapsToStringsWithFilter(
                        key,
                        digitalObject[RICO.isOrWasRegulatedBy.localName],
                        SCHEMA.sameAs.localName,
                        RICO.type.localName,
                        RICO.Types.Rule.usage
                    ),
                    access = functions.getZeroOrMoreMapsToStringsWithFilter(
                        key,
                        digitalObject[RICO.isOrWasRegulatedBy.localName],
                        RICO.name.localName,
                        RICO.type.localName,
                        RICO.Types.Rule.access
                    ).map { translationMappers.getAccessTerm(it) },
                    colour = functions.getString(key, digitalObject, RdaProperties.HAS_COLOUR_CONTENT),
                    digital = EnrichedDigitalMetadata(
                        hasFormat = functions.getString(key, digitalObject, EBUCORE.hasFormat.localName)
                            .getOrElse(0) { "" },
                        isDistributedOn = functions.getString(key, digitalObject, EBUCORE.isDistributedOn.localName)
                            .getOrElse(0) { "" },
                        hasMimeType = functions.getString(key, digitalObject, EBUCORE.hasMimeType.localName)
                            .getOrElse(0) { "" },
                        height = height,
                        width = width,
                        aspectRatio = AspectRatio.asFraction(width, height),
                        mediaResourceDescription = functions.getString(
                            key, digitalObject, EBUCORE.mediaResourceDescription.localName
                        ).getOrElse(0) { "" },
                        orientation = functions.getString(key, digitalObject, EBUCORE.orientation.localName)
                            .getOrElse(0) { "" },
                        componentColor = functions.getString(key, digitalObject, EDM.componentColor.localName)

                    ),
                    duration = functions.getString(key, digitalObject, EBUCORE.duration.localName)
                )
            } else {
                null
            }
        }

        val physicalObjectSearchDoc = globalPhysicalObject.let { physicalObject ->
            if (physicalObject != null) {
                val value = functions.getIdentifier(
                    key, physicalObject, RICO.Types.Identifier.callNumber, true
                )
                val callNumber = if (value.isEmpty()) {
                    emptyList()
                } else {
                    listOf(value)
                }
                PhysicalObjectSearchDoc(
                    access = functions.getZeroOrMoreMapsToStringsWithFilter(
                        key,
                        physicalObject[RICO.isOrWasRegulatedBy.localName],
                        RICO.name.localName,
                        RICO.type.localName,
                        RICO.Types.Rule.access
                    ).map { translationMappers.getAccessTerm(it) },
                    formats = getEnrichedValues(
                        key,
                        physicalObject[RICO.hasCarrierType.localName],
                        RICO.hasCarrierType.localName,
                        "physical object.",
                        RICO.name.localName,
                        functions,
                        errorList
                    ),
                    duration = functions.getString(
                        key, physicalObject, EBUCORE.duration.localName
                    ),
                    colour =
                    functions.getTrilingualPropertyList(
                        physicalObject, RdaProperties.HAS_COLOUR_CONTENT
                    ),
                    characteristics =
                    functions.getTrilingualPropertyList(
                        physicalObject, RICO.physicalCharacteristics.localName
                    ),
                    descriptiveNote = functions.getTrilingualPropertyList(
                        physicalObject, RICO.descriptiveNote.localName
                    ),
                    conditionsOfUse = functions.getTrilingualPropertyList(
                        physicalObject, RICO.conditionsOfUse.localName
                    ),
                    usage = functions.getZeroOrMoreMapsToStringsWithFilter(
                        key,
                        physicalObject[RICO.isOrWasRegulatedBy.localName],
                        SCHEMA.sameAs.localName,
                        RICO.type.localName,
                        RICO.Types.Rule.usage
                    ),
                    callNumber = callNumber
                )
            } else {
                null
            }
        }
        return Instantiations(
            physicalObjectSearchDoc,
            digitalObjectSearchDoc,
        )
    }


    private fun getEnrichedValues(
        key: String,
        input: Any?,
        property: String,
        parent: String,
        labelProperty: String,
        functions: SharedSearchDocUtility,
        errorList: MutableList<String>
    ): List<EnrichedFacetContainer> {
        return when (input) {
            is Map<*, *> -> {
                val message = "Value in property $property for entity $parent for record $key has no enrichment."
                log.debug(message)
                errorList.add(message)
                emptyList()
            }

            is List<*> -> input.mapNotNull { item ->
                when (item) {
                    is Map<*, *> -> {
                        if (item.containsKey(RICO.resultsOrResultedFrom.localName)) {
                            when (val resultedResource = item[RICO.resultsOrResultedFrom.localName]) {
                                is Map<*, *> -> {
                                    if (resultedResource.containsKey(RICO.affectsOrAffected.localName)) {
                                        EnrichedFacetContainer(
                                            displayLabel = functions.getTrilingualProperty(
                                                resultedResource[RICO.affectsOrAffected.localName] as Map<String, Any?>,
                                                labelProperty
                                            ),
                                            name = functions.getTrilingualProperty(
                                                item as Map<String, Any?>, labelProperty
                                            ),
                                            type = item[RICO.type.localName] as String?
                                        )
                                    } else {
                                        null
                                    }
                                }

                                else -> null
                            }

                        } else {
                            if (item.containsKey(RICO.isOrWasAffectedBy.localName)) {
                                null
                            } else {
                                val message = "Value in property $property is not enriched for record $key."
                                log.debug(message)
                                errorList.add(message)
                                null
                            }
                        }
                    }

                    else -> null
                }

            }

            else -> emptyList()
        }
    }

    private fun processAgents(
        key: String, property: String, input: Map<String, Any?>, agentTransformer: AgentTransformer
    ) {
        input[property].let { any ->
            when (any) {
                is List<*> -> {
                    any.forEach {
                        agentTransformer.add(key, property, it as Map<String, Any?>)
                    }
                }

                is Map<*, *> -> agentTransformer.add(key, property, any as Map<String, Any?>)

                else -> InvalidInputException("Could not process $property in record $key, because it is of an invalid structure.")
            }
        }
    }

    private fun processCreationRelations(key: String, input: Map<String, Any?>, agentTransformer: AgentTransformer) {
        input[RICO.recordResourceOrInstantiationIsSourceOfCreationRelation.localName].let { any ->
            when (any) {
                is List<*> -> {
                    any.forEach {
                        agentTransformer.addCreationRelation(key, it as Map<String, Any?>)
                    }
                }

                is Map<*, *> -> agentTransformer.addCreationRelation(key, any as Map<String, Any?>)

                else -> InvalidInputException("Could not process ${RICO.recordResourceOrInstantiationIsSourceOfCreationRelation.localName} in record $key, because it is of an invalid structure.")
            }
        }
    }
}