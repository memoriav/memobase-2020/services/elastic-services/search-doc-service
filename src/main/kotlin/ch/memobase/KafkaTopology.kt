/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.Constants.SettingsProps
import ch.memobase.helpers.JsonUtility
import ch.memobase.helpers.TranslationMappers
import ch.memobase.reporting.Report
import ch.memobase.reporting.ReportStatus
import ch.memobase.schemas.search.Document
import ch.memobase.settings.SettingsLoader
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.KStream
import org.apache.kafka.streams.kstream.Predicate
import org.apache.logging.log4j.LogManager

class KafkaTopology(
    private val settings: SettingsLoader,
    translationMappers: TranslationMappers,
) {
    private val log = LogManager.getLogger(this::class.java)

    private val appSettings = settings.appSettings
    private val stepName = appSettings.getProperty(SettingsProps.REPORTING_STEP_NAME)
    private val mediaUrl = appSettings.getProperty(SettingsProps.MEDIA_URL)
    private val documentSearchDocBuilder = DocumentsSearchDocTransformer(
        translationMappers,
        mediaUrl,
        stepName,
        appSettings.getProperty(SettingsProps.STEP_VERSION)
    )

    fun build(): Topology {
        val builder = StreamsBuilder()
        pipeline(
            builder.stream(settings.inputTopic),
            settings.outputTopic,
            stepName
        )
        return builder.build()
    }

    private fun pipeline(stream: KStream<String, String>, outputTopic: String, step: String) {
        val parsedJsonStream = stream
            .filter { key, value ->
                if (value == null) {
                    log.warn("Ignored message with key '$key' as the value is null.")
                    false
                } else {
                    true
                }
            }
            .mapValues { readOnlyKey, value -> JsonUtility.parseJson(readOnlyKey, value, step) }
            .branch(
                Predicate { _, value -> value.second.status == ReportStatus.fatal },
                Predicate { _, value -> value.second.status == ReportStatus.success }
            )

        parsedJsonStream[0]
            .mapValues { value -> value.second.toJson() }
            .to(settings.processReportTopic)

        val processedStream = parsedJsonStream[1]
            .mapValues { value -> value.first.map }
            .mapValues { readOnlyKey, value ->
                documentSearchDocBuilder.transform(readOnlyKey, value)
            }
        pipelineOutput(processedStream, outputTopic)
    }

    private fun pipelineOutput(outputStream: KStream<String, Pair<Document, Report>>, outputTopic: String) {
        outputStream
            .map { _, value -> KeyValue(value.second.id, value.second) }
            .mapValues { value -> value.toJson() }
            .to(settings.processReportTopic)

        outputStream
            .filter { _, value -> value.second.status != ReportStatus.fatal }
            .map { _, value -> KeyValue(value.first.id, value.first) }
            .mapValues { value -> value.toJson() }
            .to(outputTopic)
    }
}


