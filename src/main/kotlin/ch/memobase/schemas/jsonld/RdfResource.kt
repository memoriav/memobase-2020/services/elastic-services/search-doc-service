package ch.memobase.schemas.jsonld

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RdfResource(
    @SerialName("@id")
    val id: String
)