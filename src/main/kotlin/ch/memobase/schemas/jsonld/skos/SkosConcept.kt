package ch.memobase.schemas.jsonld.skos

import ch.memobase.schemas.jsonld.rico.RicoActivity
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SkosConcept(
    @SerialName("@id")
    val id: String,
    @SerialName("@type")
    val rdfType: String,


    val prefLabel: String?,
    val prefLabelDe: String?,
    val prefLabelFr: String?,
    val prefLabelIt: String?,


    val isOrWasAffectedBy: RicoActivity? = null,

    )