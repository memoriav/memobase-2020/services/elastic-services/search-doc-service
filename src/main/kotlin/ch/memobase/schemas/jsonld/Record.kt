/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schemas.jsonld

import ch.memobase.schemas.jsonld.rico.*
import ch.memobase.schemas.jsonld.skos.SkosConcept
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class Record(
    @SerialName("@id")
    val id: String,
    @SerialName("@type")
    val rdfType: String,
    @SerialName("@context")
    val context: String,

    val type: String,
    val title: String,

    val abstract: String? = null,
    val conditionsOfAccess: String? = null,
    val conditionsOfUse: String? = null,
    val descriptiveNote: String? = null,


    val created: RicoDate? = null,
    val hasGenre: List<SkosConcept>,
    val hasOrHadLanguage: List<RicoLanguage>,

    val hasInstantiation: List<RicoInstantiation>,

    val hasOrHadIdentifier: RicoIdentifier,
    val hasOrHadHolder: List<RdfResource>,
    val hasOrHadSubject: List<SkosConcept>,
    val hasOrHadTitle: List<RicoTitle>,
    val hasPlaceOfCapture: List<RicoPlace>,
    val hasProducer: List<RicoAgent>,
    val hasPublisher: List<RicoAgent>,

    val hasSponsoringAgentOfResource: String,
    val instanceOf: List<String>,
    val isOrWasPartOf: List<String>,
    val isOrWasRegulatedBy: List<RicoRule>,

    val spatial: List<RicoPlace>,
    val temporal: List<RicoDate>,

    val isPublished: Boolean,

    val issued: RicoDate,
    val relation: String,
    val sameAs: String,
    val scopeAndContent: String,
    val scopeAndContentDe: String,
    val scopeAndContentFr: String,
    val scopeAndContentIt: String,
    val recordResourceOrInstantiationIsSourceOfCreationRelation: List<RicoCreationRelation>,

    val source: String,


    )


