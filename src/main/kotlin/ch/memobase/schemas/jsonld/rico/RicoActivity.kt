package ch.memobase.schemas.jsonld.rico

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RicoActivity(
    @SerialName("@id")
    val id: String,
    @SerialName("@type")
    val rdfType: String,

    val beginningDate: String,
    val endDate: String,

    val isOrWasPerformedBy: RicoMechanism,
)