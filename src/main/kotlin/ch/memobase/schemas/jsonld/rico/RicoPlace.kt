package ch.memobase.schemas.jsonld.rico

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RicoPlace(
    @SerialName("@id")
    val id: String,
    @SerialName("@type")
    val rdfType: String,

)