/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schemas.jsonld.rico

import ch.memobase.schemas.jsonld.RdfResource
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RicoInstantiation(
    @SerialName("@id")
    val id: String,
    @SerialName("@type")
    val rdfType: String,
    val componentColor: List<String>? = null,
    val conditionsOfUse: String? = null,
    val created: RicoDate? = null,
    val duration: String? = null,
    val hasCarrierType: List<RicoCarrierType>? = null,
    val hasColourContent: String? = null,
    val hasDerivedInstantiation: RicoInstantiation? = null,
    val hasFormat: String? = null,
    val hasMimeType: String? = null,
    val hasOrHadIdentifier: RicoIdentifier? = null,
    val height: String? = null,
    val isDerivedFromInstantiation: RdfResource? = null,

    val isDistributedOn: String? = null,
    val isInstantiationOf: RdfResource,
    val isOrWasRegulatedBy: List<RicoRule>? = null,
    val locator: String? = null,
    val mediaLocation: String? = null,
    val orientation: String? = null,
    val physicalCharacteristics: String? = null,
    val proxyType: String? = null,
    val type: String,
    val width: String? = null,
)
