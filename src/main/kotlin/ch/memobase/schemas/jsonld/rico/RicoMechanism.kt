package ch.memobase.schemas.jsonld.rico

import ch.memobase.schemas.jsonld.RdfResource
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RicoMechanism(
    @SerialName("@id")
    val id: String,
    @SerialName("@type")
    val rdfType: String,


    val name: String,
    val performsOrPerformed: RdfResource,

    )