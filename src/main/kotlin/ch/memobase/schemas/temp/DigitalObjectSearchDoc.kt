/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schemas.temp

import ch.memobase.schemas.search.EnrichedDigitalMetadata
import ch.memobase.schemas.search.FacetContainer
import ch.memobase.schemas.search.LanguageContainer

data class DigitalObjectSearchDoc(
    val access: List<FacetContainer>,
    val digital: EnrichedDigitalMetadata,
    val duration: List<String>,
    val locator: String?,
    val mediaLocation: String?,
    val colour: List<String>,
    val conditionsOfUse: List<LanguageContainer>,
    val usageDigital: List<String>,
    val descriptiveNote: List<LanguageContainer>
)
