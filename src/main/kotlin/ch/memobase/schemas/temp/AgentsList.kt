package ch.memobase.schemas.temp

import ch.memobase.schemas.search.AgentWithRelationContainer

data class AgentsList(
    val subject: MutableList<AgentWithRelationContainer> = mutableListOf(),
    val publisher: MutableList<AgentWithRelationContainer> = mutableListOf(),
    val producer: MutableList<AgentWithRelationContainer> = mutableListOf(),
    val creator: MutableList<AgentWithRelationContainer> = mutableListOf(),
    val contributor: MutableList<AgentWithRelationContainer> = mutableListOf()
)