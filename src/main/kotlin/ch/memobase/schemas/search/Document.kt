/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schemas.search

import ch.memobase.helpers.Date
import com.beust.klaxon.Klaxon
import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_EMPTY)
data class Document(
    val id: String,
    val lastUpdatedDate: String,
    // Titles
    val title: List<LanguageContainer>,
    val seriesTitle: List<LanguageContainer>,
    val broadcastTitle: List<LanguageContainer>,

    // Datatype properties
    val type: FacetContainer,
    val sourceID: String,
    val oldMemobaseId: String,
    val abstract: List<LanguageContainer>,
    val descriptiveNote: List<LanguageContainer>,
    val scopeAndContent: List<LanguageContainer>,
    val source: List<LanguageContainer>,
    val relatedMaterial: List<LanguageContainer>,
    val rightsHolder: List<LanguageContainer>,
    val conditionsOfUse: List<LanguageContainer>,
    val sameAs: List<String>,

    // Concepts
    val format: List<EnrichedFacetContainer>,
    val genre: List<EnrichedFacetContainer>,
    val language: List<EnrichedFacetContainer>,
    val keywords: LanguageContainer,

    // Agents
    val personSubject: List<AgentWithRelationContainer>,
    val personCreator: List<AgentWithRelationContainer>,
    val personContributor: List<AgentWithRelationContainer>,
    val personPublisher: List<AgentWithRelationContainer>,
    val personProducer: List<AgentWithRelationContainer>,
    val personsFacet: SimpleFacetContainer,

    val corporateBodySubject: List<AgentWithRelationContainer>,
    val corporateBodyCreator: List<AgentWithRelationContainer>,
    val corporateBodyContributor: List<AgentWithRelationContainer>,
    val corporateBodyPublisher: List<AgentWithRelationContainer>,
    val corporateBodyProducer: List<AgentWithRelationContainer>,

    val agentSubject: List<AgentWithRelationContainer>,
    val agentCreator: List<AgentWithRelationContainer>,
    val agentContributor: List<AgentWithRelationContainer>,
    val agentPublisher: List<AgentWithRelationContainer>,
    val agentProducer: List<AgentWithRelationContainer>,

    // Places
    val placeRelated: List<FacetContainer>,
    val placeCapture: List<FacetContainer>,
    val placeRelatedEnriched: List<FacetContainer>,
    val placeCaptureEnriched: List<FacetContainer>,
    val placeFacet: SimpleFacetContainer,
    val placeFacetEnriched: SimpleLanguageFacetContainer,
    val enrichedPlacesCaptureMetadata: List<EnrichedPlaceMetadata>,
    val enrichedPlacesRelatedMetadata: List<EnrichedPlaceMetadata>,
    // Dates
    val temporal: List<DateSearchField>,
    val dateCreated: List<DateSearchField>,
    val dateIssued: List<DateSearchField>,
    val dateFacetField: List<DateFacetField>,

    // Relations
    val institution: List<FacetContainer>,
    val recordSet: FacetContainer,

    val memoriavClaim: Boolean,

    // Digital Object
    val durationDigital: List<String>,
    val colourDigital: List<String>,
    val locator: String?,
    val mediaLocation: String?,
    val accessDigital: List<FacetContainer>,
    val usageDigital: List<String>,
    val usageDigitalGroup: List<EnrichedFacetContainer>,
    val digitalObjectNote: List<LanguageContainer>,
    val usageConditionsDigital: List<LanguageContainer>,

    // enriched from digital object
    val digital: EnrichedDigitalMetadata,

    // Physical Object
    val durationPhysical: List<String>,
    val callNumber: List<String>,
    val accessPhysical: List<FacetContainer>,
    val usagePhysical: List<String>,
    val physicalCharacteristics: List<LanguageContainer>,
    val colourPhysical: List<LanguageContainer>,
    val usageConditionsPhysical: List<LanguageContainer>,
    val physicalObjectNote: List<LanguageContainer>,

    // Combined Physical & Digital
    val access: List<EnrichedFacetContainer>,

    val accessInstitution: List<FacetContainer>,
    val originalInstitution: List<FacetContainer>,
    val masterInstitution: List<FacetContainer>,

    // internal fields
    val published: Boolean,

    // auto complete source
    val suggest: SuggestContainer,
) {
    fun toJson(): String {
        return Klaxon().toJsonString(this)
    }

    companion object {
        private val otherDocumentTypeLanguageContainer =
            LanguageContainer(listOf("Andere"), listOf("Autres"), listOf("Altri"))

        val DEFAULT = Document(
            "Default",
            Date.now(),
            emptyList(),
            emptyList(),
            emptyList(),
            FacetContainer(
                otherDocumentTypeLanguageContainer,
                "Andere",
                emptyList()
            ),
            "NoSourceIdFound",
            "",
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            LanguageContainer.EMPTY,
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            SimpleFacetContainer.EMPTY,
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            SimpleFacetContainer.EMPTY,
            SimpleLanguageFacetContainer.EMPTY,
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            FacetContainer.EMPTY,
            false,
            emptyList(),
            emptyList(),
            null,
            null,
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            EnrichedDigitalMetadata(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            false,
            SuggestContainer(emptyList(), emptyList(), emptyList(), emptyList()),
        )
    }
}
