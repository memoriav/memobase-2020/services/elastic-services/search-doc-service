package ch.memobase.schemas.search

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_EMPTY)
class EnrichedPlaceMetadata(
    val name: LanguageContainer,
    val description: LanguageContainer,
    val coordinates: Coordinates?,
    val coordinatesOfGeographicCenter: Coordinates?,
    val sameAs: List<String>,
    val wikidata: String?,
    val gnd: String?,
    val hls: String?,
)