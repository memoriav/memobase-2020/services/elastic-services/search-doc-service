/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schemas.search

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude

@Suppress("UNCHECKED_CAST")
@JsonInclude(JsonInclude.Include.NON_NULL)
data class LanguageContainer(
    val de: List<String> = emptyList(),
    val fr: List<String> = emptyList(),
    val it: List<String> = emptyList()
) {
    companion object {
        val EMPTY = LanguageContainer()

        fun placeholder(placeholder: String): LanguageContainer {
            return LanguageContainer(listOf(placeholder), listOf(placeholder), listOf(placeholder))
        }

        fun fromMap(map: Any?): LanguageContainer {
            return when (map) {
                is Map<*, *> -> {
                    val languageContainer = LanguageContainer(
                        extractMapItem(map as Map<String, Any?>, "de"),
                        extractMapItem(map, "fr"),
                        extractMapItem(map, "it")
                    )
                    languageContainer
                }
                else -> LanguageContainer()
            }
        }

        private fun extractMapItem(map: Map<String, Any?>, language: String): List<String> {
            return when (val item = map[language]) {
                is String -> listOf(item)
                is List<*> ->
                    try {
                        item as List<String>
                    } catch (ex: ClassCastException) {
                        emptyList()
                    }
                else -> emptyList()
            }
        }

    }

    fun addSingleText(input: String): LanguageContainer {
        return LanguageContainer((de + input).toSet().toList(), (fr + input).toSet().toList(), (it + input).toSet().toList())
    }

    fun addListOfTexts(input: List<String>): LanguageContainer {
        return LanguageContainer((de + input).toSet().toList(), (fr + input).toSet().toList(), (it + input).toSet().toList())
    }

    fun toList(): List<String> {
        return de + fr + it
    }

    fun toSet(): List<String> {
        return (de + fr + it).toSet().toList()
    }

    fun merge(container: LanguageContainer): LanguageContainer {
        return LanguageContainer(
            de + container.de,
            fr + container.fr,
            it + container.it
        )
    }

    fun any(): String {
        return when {
            de.isNotEmpty() -> de[0]
            fr.isNotEmpty() -> fr[0]
            it.isNotEmpty() -> it[0]
            else -> ""
        }
    }

    @JsonIgnore
    fun isEmpty(): Boolean {
        return de.isEmpty() && fr.isEmpty() && it.isEmpty()
    }

    /**
     * This function is used to remove the prefix added to certain labels to enforce a specific order. This
     * prefix needs to be removed from display stuff.
     */
    fun removePrefix(): LanguageContainer {
        return LanguageContainer(de.map { it.removeRange(0, 2) },
            fr.map { it.removeRange(0, 2) },
            it.map { it.removeRange(0, 2) })
    }
}
