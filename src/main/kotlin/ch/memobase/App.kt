/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.Constants.SettingsProps
import ch.memobase.settings.SettingsLoader
import org.apache.logging.log4j.LogManager
import kotlin.system.exitProcess

class App {
    companion object {
        private val log = LogManager.getLogger(this::class.java)
        fun createSettings(file: String): SettingsLoader {
            return SettingsLoader(
                listOf(
                    SettingsProps.DOCUMENT_TYPE_LABELS_PATH,
                    SettingsProps.ACCESS_TERM_LABELS_PATH,
                    SettingsProps.REUSE_STATEMENT_LABELS_PATH,
                    SettingsProps.REUSE_STATEMENTS_DISPLAY_LABELS_PATH,
                    SettingsProps.MEDIA_URL,
                    SettingsProps.REPORTING_STEP_NAME,
                    SettingsProps.STEP_VERSION
                ),
                file,
                useStreamsConfig = true
            )
        }

        @JvmStatic
        fun main(@Suppress("unused") args: Array<String>) {
            try {
                Service(createSettings("app.yml")).run()
            } catch (ex: Exception) {
                ex.printStackTrace()
                log.error("Stopping application due to error: " + ex.message)
                exitProcess(1)
            }
        }
    }
}
