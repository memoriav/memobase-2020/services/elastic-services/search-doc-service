/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.helpers

import ch.memobase.schemas.search.EnrichedFacetContainer
import ch.memobase.schemas.search.FacetContainer
import ch.memobase.schemas.search.LanguageContainer

class TranslationMappers(
    documentTypePath: String,
    accessTermPath: String,
    reuseStatementPath: String,
    reuseStatementDisplayLabelPath: String
) {
    private val documentTypeLabels = LoadFile.readLabelFile(documentTypePath)
    private val accessTermLabels = LoadFile.readLabelFile(accessTermPath)
    private val reuseStatementLabels = LoadFile.readLabelFileLanguageContainer(reuseStatementPath)
    private val reuseStatementDisplayLabels = LoadFile.readLabelFileLanguageContainer(reuseStatementDisplayLabelPath)

    fun getDocumentType(code: String): FacetContainer {
        return documentTypeLabels[code] ?: FacetContainer.EMPTY
    }

    fun getAccessTerm(code: String): FacetContainer {
        return accessTermLabels[code] ?: FacetContainer.EMPTY
    }

    fun getReuseStatement(uri: String): EnrichedFacetContainer {
        return EnrichedFacetContainer(
            reuseStatementDisplayLabels[uri] ?: LanguageContainer.EMPTY,
            reuseStatementLabels[uri] ?: LanguageContainer.EMPTY,
            uri
        )
    }
}