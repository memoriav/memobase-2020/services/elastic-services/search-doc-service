/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.helpers

import ch.memobase.reporting.Report
import ch.memobase.reporting.ReportStatus
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Klaxon
import com.beust.klaxon.KlaxonException
import org.apache.logging.log4j.LogManager
import java.io.StringReader

object JsonUtility {
    private val log = LogManager.getLogger(this::class.java)
    private val klaxon = Klaxon()

    fun parseJson(key: String, data: String, step: String): Pair<JsonObject, Report> {
        return try {
            Pair(
                klaxon.parseJsonObject(StringReader(data)),
                Report(key, ReportStatus.success, "Successfully parsed input document.", step, "test")
            )
        } catch (ex: KlaxonException) {
            log.error(ex.localizedMessage)
            Pair(JsonObject(), Report(key, ReportStatus.fatal, "JSON Parse Error: ${ex.localizedMessage}.", step, "test"))
        } catch (ex: ClassCastException) {
            log.error(ex.localizedMessage)
            Pair(JsonObject(), Report(key, ReportStatus.fatal, "JSON Parse Error: ${ex.localizedMessage}.", step, "test"))
        }
    }
}