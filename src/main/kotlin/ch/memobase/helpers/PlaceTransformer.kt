/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.helpers

import ch.memobase.Constants
import ch.memobase.Constants.FACET_LEVEL_0
import ch.memobase.Constants.FACET_LEVEL_1
import ch.memobase.Constants.FACET_SEPARATOR_VALUE
import ch.memobase.rdf.RICO
import ch.memobase.rdf.SCHEMA
import ch.memobase.schemas.search.*
import org.apache.logging.log4j.LogManager

@Suppress("UNCHECKED_CAST")
class PlaceTransformer(private val functions: SharedSearchDocUtility) {
    private val log = LogManager.getLogger(this::class.java)
    private val errorList = mutableListOf<String>()


    val places = mutableMapOf<String, MutableList<FacetContainer>>()
    val enrichedPlaces = mutableMapOf<String, MutableList<FacetContainer>>()

    val enrichedPlaceMetadata = mutableMapOf<String, MutableList<EnrichedPlaceMetadata>>()
    private val placeFacetValues = mutableSetOf<String>()
    private val placeNames = mutableSetOf<String>()

    private val placeFacetValuesEnrichedDe = mutableSetOf<String>()
    private val placeNamesEnrichedDe = mutableSetOf<String>()
    private val placeFacetValuesEnrichedFr = mutableSetOf<String>()
    private val placeNamesEnrichedFr = mutableSetOf<String>()
    private val placeFacetValuesEnrichedIt = mutableSetOf<String>()
    private val placeNamesEnrichedIt = mutableSetOf<String>()

    fun add(key: String, values: Any?, property: String) {
        when (values) {
            is List<*> -> {
                values.forEach {
                    when (it) {
                        is Map<*, *> -> {
                            if (it.containsKey(RICO.resultsOrResultedFrom.localName)) {
                                processEnrichedPlace(key, it as Map<String, Any?>, property)
                            } else {
                                processPlace(key, it as Map<String, Any?>, property)
                            }
                        }

                        else -> errorList.add("Found object with invalid type in list of property $property in record $key.")
                    }
                }
            }

            is Map<*, *> -> {
                if (values.containsKey(RICO.resultsOrResultedFrom.localName)) {
                    processEnrichedPlace(key, values as Map<String, Any?>, property)
                } else {
                    processPlace(key, values as Map<String, Any?>, property)
                }
            }

            else -> {
                errorList.add("No places added for $property on record $key.")
            }
        }
    }

    private fun processPlace(key: String, values: Map<String, Any?>, property: String) {
        val names = functions.getTrilingualProperty(values, RICO.name.localName)
        if (names.isEmpty()) {
            errorList.add("Place without a name for $property on record $key.")
            log.error("Place without a name for $property on record $key.")
        } else {
            places[property] = places[property].let { currentContainer ->
                val facetValues = names.toSet().flatMap { name ->
                    facetValues(name)
                }
                placeNames.addAll(names.toList())
                placeFacetValues.addAll(facetValues)
                if (currentContainer != null) {
                    currentContainer.add(
                        FacetContainer(
                            names, names.any(), facetValues.toSet().sorted()
                        )
                    )
                    currentContainer
                } else {
                    mutableListOf(
                        FacetContainer(
                            names, names.any(), facetValues.toSet().sorted()
                        )
                    )
                }
            }
        }
    }

    private fun processEnrichedPlace(key: String, values: Map<String, Any?>, property: String) {
        val names = functions.getTrilingualProperty(values, RICO.name.localName)
        val description = functions.getTrilingualProperty(values, RICO.descriptiveNote.localName)
        val coordinates =
            functions.getString(key, values, Constants.FramedWikidataProperties.COORDINATES).firstOrNull().let { coordinateValue ->
                if (coordinateValue != null) {
                    extractCoordinatePoint(key, property, coordinateValue)
                } else {
                    null
                }
            }
        val coordinatesOfGeographicCenter =
            functions.getString(key, values, Constants.FramedWikidataProperties.COORDINATES_OF_GEOGRAPHIC_CENTER)
                .firstOrNull().let { coordinateValue ->
                    if (coordinateValue != null) {
                        extractCoordinatePoint(key, property, coordinateValue)
                    } else {
                        null
                    }
                }

        val sameAs = functions.getString(key, values, SCHEMA.sameAs.localName)

        // have different fields for different languages.
        if (names.isEmpty()) {
            errorList.add("Enriched place without a name for $property on record $key.")
            log.error("Enriched place without a name for $property on record $key.")
        } else {
            enrichedPlaceMetadata[property] = enrichedPlaceMetadata[property].let { currentContainer ->
                val enrichedPlaceMetadata = EnrichedPlaceMetadata(
                    name = names,
                    description = description,
                    coordinates = coordinates,
                    coordinatesOfGeographicCenter = coordinatesOfGeographicCenter,
                    sameAs = sameAs,
                    wikidata = sameAs.firstOrNull { x -> x.startsWith("https://www.wikidata.org/entity/") },
                    gnd = sameAs.firstOrNull { x -> x.startsWith("https://d-nb.info/gnd/") },
                    hls = sameAs.firstOrNull { x -> x.startsWith("https://hls-dhs-dss.ch/articles/") },
                )
                if (currentContainer != null) {
                    currentContainer.add(enrichedPlaceMetadata)
                    currentContainer
                } else {
                    mutableListOf(enrichedPlaceMetadata)
                }
            }
            enrichedPlaces[property] = enrichedPlaces[property].let { currentContainer ->
                val facetValuesDe = names.de.flatMap {
                    facetValues(it)
                }
                placeNamesEnrichedDe.addAll(names.de)
                placeFacetValuesEnrichedDe.addAll(facetValuesDe)

                val facetValuesFr = names.fr.flatMap {
                    facetValues(it)
                }
                placeNamesEnrichedFr.addAll(names.fr)
                placeFacetValuesEnrichedFr.addAll(names.fr.flatMap {
                    facetValues(it)
                })

                val facetValuesIt = names.it.flatMap {
                    facetValues(it)
                }
                placeNamesEnrichedIt.addAll(names.it)
                placeFacetValuesEnrichedIt.addAll(facetValuesIt)
                if (currentContainer != null) {
                    currentContainer.add(
                        FacetContainer(
                            names, names.any(), facetValuesDe + facetValuesFr + facetValuesIt.toSet().sorted()
                        )
                    )
                    currentContainer
                } else {
                    mutableListOf(
                        FacetContainer(
                            names, names.any(), facetValuesDe + facetValuesFr + facetValuesIt.toSet().sorted()
                        )
                    )
                }
            }
        }

    }

    private fun facetValues(name: String): List<String> {
        val capitalLetter = Utility.getCapitalLetter(name)
        return listOf(
            "${FACET_LEVEL_0}${FACET_SEPARATOR_VALUE}$capitalLetter${FACET_SEPARATOR_VALUE}",
            "${FACET_LEVEL_1}${FACET_SEPARATOR_VALUE}$capitalLetter${FACET_SEPARATOR_VALUE}$name${FACET_SEPARATOR_VALUE}"
        )
    }

    fun getCombinedPlaceFacet(): SimpleFacetContainer {
        return SimpleFacetContainer(
            placeFacetValues.toList().sortedBy { v -> v.substring(2) }, placeNames.toList().sorted()
        )
    }

    fun getCombinedPlaceFacetEnriched(): SimpleLanguageFacetContainer {
        return SimpleLanguageFacetContainer(
            de = SimpleFacetContainer(
                placeFacetValuesEnrichedDe.toList().sortedBy { v -> v.substring(2) },
                placeNamesEnrichedDe.toList().sorted()
            ),
            fr = SimpleFacetContainer(
                placeFacetValuesEnrichedFr.toList().sortedBy { v -> v.substring(2) },
                placeNamesEnrichedFr.toList().sorted()
            ),
            it = SimpleFacetContainer(
                placeFacetValuesEnrichedIt.toList().sortedBy { v -> v.substring(2) },
                placeNamesEnrichedIt.toList().sorted()
            )
        )
    }

    private fun extractCoordinatePoint(key: String, property: String, value: String): Coordinates? {
        val coord = value.trim('(', ')', 'P', 'o', 'i', 'n', 't').split(" ")
        return if (coord.size == 2) {
            Coordinates(coord[0], coord[1])
        } else {
            val message = "Invalid coordinates for $property on record $key with value $coord."
            errorList.add(message)
            log.error(message)
            null
        }
    }
}