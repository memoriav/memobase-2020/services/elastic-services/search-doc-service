/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.helpers

import ch.memobase.rdf.RICO
import org.apache.logging.log4j.LogManager

object Utility {
    private val log = LogManager.getLogger(this::class.java)
    private val isAlphaChar = Regex("[A-Za-z]")

    fun getCapitalLetter(name: String): Char {
        val foldedName = AsciiFolder.foldToASCII(name)
        return try {
            val firstChar = foldedName.first { isAlphaChar.matches(it.toString()) }
            firstChar.uppercaseChar()
        } catch (ex: NoSuchElementException) {
            log.error("The name does not contain a valid alpha character: $foldedName.")
            'Z'
        }
    }

    fun sortRecordSets(key: String, input: Map<String, Any?>): Pair<List<String>, Any?> {
        return when (val values = input[RICO.isOrWasHolderOf.localName]) {
            is Map<*, *> ->
                if (values.containsKey("@id")) {
                    val id = (values["@id"] as String).substringAfterLast(":")
                    if (id.endsWith("avk")) {
                        Pair(listOf(), values)
                    } else {
                        Pair(listOf(id), null)
                    }
                } else {
                    Pair(emptyList(), null)
                }

            is List<*> -> {
                var collection: Any? = null
                val recordSets = mutableListOf<String>()
                values.forEach {
                    when (it) {
                        is Map<*, *> -> {
                            if (it.containsKey("@id")) {
                                val id = (it["@id"] as String).substringAfterLast(":")
                                if (id.endsWith("avk")) {
                                    collection = it
                                } else {
                                    recordSets.add(id)
                                }
                            } else {
                                log.error(
                                    "Institution $key holds a record set without id. " +
                                            "This really shouldn't be possible."
                                )
                            }
                        }

                        else -> throw InvalidInputException(
                            "Institution $key isHolderOf contains " +
                                    "unexpected value $values."
                        )
                    }
                }
                Pair(recordSets, collection)
            }

            else -> {
                Pair(emptyList(), null)
            }
        }
    }
}
