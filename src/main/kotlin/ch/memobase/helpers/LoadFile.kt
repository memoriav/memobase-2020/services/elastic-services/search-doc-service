/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.helpers

import ch.memobase.schemas.search.FacetContainer
import ch.memobase.schemas.search.LanguageContainer
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import java.io.File

object LoadFile {
    private val csv = csvReader()

    fun readLabelFile(path: String): Map<String, FacetContainer> {
        val labelList = csv.readAll(File(path))
        val labelsMap = mutableMapOf<String, FacetContainer>()

        for (row in labelList.listIterator(1)) {
            labelsMap[row[0]] = FacetContainer(LanguageContainer(listOf(row[1].trim()), listOf(row[2].trim()), listOf(row[3].trim())), row[0], emptyList())
        }
        return labelsMap
    }

    fun readLabelFileLanguageContainer(path: String): Map<String, LanguageContainer> {
        val labelList = csv.readAll(File(path))
        val labelsMap = mutableMapOf<String, LanguageContainer>()

        for (row in labelList.listIterator(1)) {
            labelsMap[row[0]] = LanguageContainer(listOf(row[1].trim()), listOf(row[2].trim()), listOf(row[3].trim()))
        }
        return labelsMap
    }

}