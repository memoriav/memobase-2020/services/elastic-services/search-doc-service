/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.helpers

import ch.memobase.Constants
import ch.memobase.schemas.search.DateFacetField
import ch.memobase.schemas.search.DateSearchField
import ch.memobase.schemas.search.LanguageContainer
import ch.memobase.rdf.NS
import ch.memobase.rdf.RICO
import org.apache.logging.log4j.LogManager

@Suppress("UNCHECKED_CAST")
class DateTransformer {
    private val log = LogManager.getLogger(this::class.java)
    private val errorList = mutableListOf<String>()
    private val dateFacetBuilder = DateFacetBuilder()
    private val singleDate = NS.namespaceToPrefix(NS.rico) + ":" + RICO.SingleDate.localName
    private val dateRange = NS.namespaceToPrefix(NS.rico) + ":" + RICO.DateRange.localName
    val dateSearchFields = mutableMapOf<String, MutableList<DateSearchField>>()
    val dateFacetFields = mutableListOf<DateFacetField>()

    fun add(key: String, values: Any?, property: String, addToFacet: Boolean) {
        when (values) {
            is List<*> -> {
                values.forEach {
                    when (it) {
                        is Map<*, *> -> processDateMap(it as Map<String, Any?>, key, property, addToFacet)
                        else -> {
                            val message = "Unexpected structure in date field for record $key."
                            log.error(message)
                            errorList.add(message)
                        }
                    }
                }
            }

            is Map<*, *> -> {
                processDateMap(values as Map<String, Any?>, key, property, addToFacet)
            }

            else -> {
                errorList.add("No dates added for $property on record $key.")
            }
        }
    }

    fun getErrors(): List<String> {
        return errorList + dateFacetBuilder.errorList
    }

    private fun processDateMap(value: Map<String, Any?>, key: String, property: String, addToFacet: Boolean) {
        val isNormalized = value.containsKey(RICO.normalizedDateValue.localName)
        val date = if (isNormalized) {
            value[RICO.normalizedDateValue.localName] as String
        } else {
            if (value.containsKey(RICO.expressedDate.localName)) value[RICO.expressedDate.localName] as String
            else {
                log.error("Date entity without a date in record $key.")
                throw InvalidInputException("Date entity without a date in record $key.")
            }
        }

        val sort = if (isNormalized) {
            date
        } else {
            null
        }

        val qualifier = when (val item = value[RICO.dateQualifier.localName]) {
            is String -> listOf(item)
            is List<*> -> item.filterIsInstance<String>()
            else -> emptyList()
        }

        val certainty = when (val item = value[RICO.certainty.localName]) {
            is String -> listOf(item)
            is List<*> -> item.filterIsInstance<String>()
            else -> emptyList()
        }

        if (dateSearchFields.containsKey(property)) {
            dateSearchFields[property]?.add(
                DateSearchField(
                    date = date, qualifier = qualifier, certainty = certainty
                )
            )
        } else {
            dateSearchFields[property] = mutableListOf(
                DateSearchField(
                    date = date, qualifier = qualifier, certainty = certainty
                )
            )
        }

        if (addToFacet) {
            val facetLists = listOf("de", "fr", "it").map {
                collectFacetStrings(
                    value, isNormalized, date, key, it
                )
            }
            dateFacetFields.add(
                DateFacetField(
                    display = date, type = property, sort = sort, facet = LanguageContainer(
                        de = facetLists[0], fr = facetLists[1], it = facetLists[2]
                    )

                )
            )
        }
    }

    private fun collectFacetStrings(
        value: Map<String, Any?>, isNormalized: Boolean, date: String, key: String, language: String
    ): List<String> {
        return when (val type = value[Constants.RDF_TYPE]) {
            singleDate -> if (isNormalized) dateFacetBuilder.buildFromNormalizedSingleDate(
                date, language
            )
            else emptyList()

            dateRange -> if (isNormalized) {
                try {
                    dateFacetBuilder.buildFromNormalizedDateRange(date, language)
                } catch (ex: NumberFormatException) {
                    log.error("Could not parse normalized date $date in resource $key.")
                    errorList.add("Could not parse normalized date $date in resource $key.")
                    emptyList()
                }
            } else {
                emptyList()
            }

            else -> {
                log.error("Could not process date as it is of rdf:type $type. (should be rico:DateRange or rico:SingleDate).")
                errorList.add("Could not process date as it is of rdf:type $type. (should be rico:DateRange or rico:SingleDate).")
                emptyList()
            }
        }
    }

}