/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.helpers

import ch.memobase.schemas.search.LanguageContainer
import ch.memobase.rdf.RICO
import org.apache.logging.log4j.LogManager

@Suppress("UNCHECKED_CAST")
class SharedSearchDocUtility(private val entity: String) {
    private val log = LogManager.getLogger(this::class.java)
    val errorList = mutableListOf<String>()
    fun getIdentifier(key: String, input: Map<String, Any?>, type: String, isOptional: Boolean): String {
        return input[RICO.hasOrHadIdentifier.localName].let { identifiers ->
            when (identifiers) {
                is List<*> -> {
                    identifiers.firstOrNull { item -> (item as Map<*, *>)[RICO.type.localName] == type }.let { first ->
                        when (first) {
                            is Map<*, *> ->
                                when (val identifier = first[RICO.identifier.localName]) {
                                    is String -> identifier
                                    is Map<*, *> ->
                                        throw InvalidInputException("Expected a string for the identifier! ${first[RICO.identifier.localName]}")
                                    is List<*> ->
                                        throw InvalidInputException("Expected a string for the identifier! ${first[RICO.identifier.localName]}")
                                    null -> {
                                        throw InvalidInputException("No identifier field in identifier entity for type '$type' present. This is required.")
                                    }
                                    else -> {
                                        throw InvalidInputException("Identifier field has an unexpected type: $identifier.")
                                    }
                                }
                            null -> {
                                if (isOptional) ""
                                else throw InvalidInputException("No $type identifier present for $entity $key which is required!")
                            }
                            else -> {
                                throw InvalidInputException("Identifier is not a map! $first")
                            }
                        }
                    }
                }

                is Map<*, *> -> {
                    val identifier = identifiers[RICO.identifier.localName] as String
                    if (identifiers[RICO.type.localName] == type) {
                        identifier
                    } else {
                        if (isOptional) ""
                        else throw InvalidInputException("No $type identifier present for $entity $key which is required!")
                    }
                }

                else -> {
                    if (isOptional) ""
                    else throw InvalidInputException("No $type identifier present for $entity $key which is required!")
                }
            }
        }
    }

    fun getTitle(key: String, input: Map<String, Any?>, type: String, parent: String): List<LanguageContainer> {
        return input[RICO.hasOrHadTitle.localName].let { titleEntity ->
            when (titleEntity) {
                is List<*> -> titleEntity.filterIsInstance<Map<String, Any?>>()
                    .firstOrNull { anyMap -> anyMap[RICO.type.localName] == type }.let { anyMap ->
                        if (anyMap != null) getTrilingualProperty(anyMap, RICO.title.localName)
                        else null
                    }.let {
                        if (it == null) emptyList()
                        else listOf(it)
                    }

                is Map<*, *> -> if (titleEntity[RICO.type.localName] == type) {
                    getTrilingualProperty(
                        titleEntity as Map<String, Any?>, RICO.title.localName
                    ).let {
                        listOf(it)
                    }
                } else {
                    log.error("No $type title found on $parent $key.")
                    errorList.add("No $type title found on $parent $key.")
                    emptyList()
                }

                else -> {
                    log.error("No $type title found on $parent $key.")
                    errorList.add("No $type title found on $parent $key.")
                    emptyList()
                }
            }
        }
    }

    fun getBoolean(key: String, input: Map<String, Any?>, property: String, parent: String): Boolean {
        return input[property].let {
            when (it) {
                is Boolean -> it
                is String -> it.toBoolean()
                else -> {
                    val message = "Found no $property property on $parent $key. Set to false."
                    log.debug(message)
                    errorList.add(message)
                    false
                }
            }
        }
    }

    fun getString(key: String, input: Map<String, Any?>, property: String): List<String> {
        return input[property].let {
            when (it) {
                is List<*> -> {
                    it.filterIsInstance<String>()
                }

                is String -> listOf(it)
                else -> {
                    if (input.containsKey(property)) {
                        val message =
                            "The property $property is malformed for $entity $key (expected a string or a list of strings)."
                        log.error(message)
                        errorList.add(message)
                        emptyList()
                    } else {
                        val message = "The property $property is not defined for $entity $key."
                        log.debug(message)
                        emptyList()
                    }

                }
            }
        }
    }

    fun getZeroOrMoreMapsToStringsWithFilter(
        key: String,
        items: Any?,
        property: String,
        filterProperty: String,
        filterValue: String
    ): List<String> {
        return when (items) {
            is Map<*, *> -> {
                if (items[filterProperty] == filterValue) {
                    getString(
                        key, items as Map<String, Any?>, property
                    )
                } else emptyList()
            }

            is List<*> -> items.flatMap { item ->
                when (item) {
                    is Map<*, *> -> {
                        if (item[filterProperty] == filterValue) {
                            getString(
                                key, item as Map<String, Any?>, property
                            )
                        } else {
                            emptyList()
                        }
                    }

                    else -> emptyList()
                }

            }

            else -> emptyList()
        }
    }

    fun getTrilingualPropertyList(input: Map<String, Any?>, base: String): List<LanguageContainer> {
        val container = getTrilingualProperty(input, base)
        return if (container.isEmpty()) {
            emptyList()
        } else {
            listOf(container)
        }
    }

    fun getTrilingualProperty(input: Map<String, Any?>, base: String): LanguageContainer {
        val withLanguage = LanguageContainer(
            de = when (val value = input[base + "_de"]) {
                is String -> listOf(value)
                is List<*> -> value.map { actualValue ->
                    if (actualValue is String)
                        actualValue
                    else
                        throw InvalidInputException("The property $base with language tag de, does not contain a string.")
                }

                else -> {
                    emptyList()
                }
            },
            fr = when (val value = input[base + "_fr"]) {
                is String -> listOf(value)
                is List<*> -> value.map { actualValue ->
                    if (actualValue is String)
                        actualValue
                    else
                        throw InvalidInputException("The property $base with language tag fr, does not contain a string.")
                }

                else -> {
                    emptyList()
                }
            },
            it = when (val value = input[base + "_it"]) {
                is String -> listOf(value)
                is List<*> -> value.map { actualValue ->
                    if (actualValue is String)
                        actualValue
                    else
                        throw InvalidInputException("The property $base with language tag it, does not contain a string.")
                }

                else -> {
                    emptyList()
                }
            }
        )
        return when (val value = input[base]) {
            is String ->
                withLanguage.addSingleText(value)

            is List<*> -> {
                if (value[0] !is String) {
                    throw InvalidInputException("The property $base does not contain a string.")
                }
                withLanguage.addListOfTexts(value as List<String>)
            }

            else -> {
                withLanguage
            }
        }
    }

    fun getZeroOneOrMoreMapsWithFilter(
        items: Any?, property: String, filterProperty: String, filterValue: String
    ): List<LanguageContainer> {
        return when (items) {
            is Map<*, *> -> {
                if (items[filterProperty] == filterValue) {
                    listOf(
                        getTrilingualProperty(
                            items as Map<String, Any?>, property
                        )
                    )
                } else emptyList()
            }

            is List<*> -> items.mapNotNull { item ->
                when (item) {
                    is Map<*, *> -> {
                        if (item[filterProperty] == filterValue) {
                            getTrilingualProperty(
                                item as Map<String, Any?>, property
                            )
                        } else {
                            null
                        }
                    }

                    else -> null
                }

            }

            else -> emptyList()
        }
    }
}