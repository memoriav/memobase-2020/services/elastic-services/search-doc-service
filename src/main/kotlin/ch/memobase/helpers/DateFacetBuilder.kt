/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.helpers

import ch.memobase.Constants.FACET_LEVEL_0
import ch.memobase.Constants.FACET_LEVEL_1
import ch.memobase.Constants.FACET_SEPARATOR_VALUE
import org.apache.logging.log4j.LogManager

class DateFacetBuilder {
    private val log = LogManager.getLogger(this::class.java)
    val errorList = mutableListOf<String>()

    private val centuryNames = mapOf(
        Pair("de", "Jahrhundert"),
        Pair("fr", "siècle"),
        Pair("it", "secolo")
    )

    /**
     * Builds the hierarchical facet for a normalized SingleDate date.
     *
     * @param date Normalized date value with structure YYYY-MM-DD.
     *
     * @return The facet values to construct the hierarchy with century and decade.
     */
    fun buildFromNormalizedSingleDate(date: String, language: String): List<String> {
        val century = getCentury(date.substring(0, 4), language)
        val decade = getDecade(date.substring(0, 4))
        return listOf(
            "$FACET_LEVEL_0$FACET_SEPARATOR_VALUE$century$FACET_SEPARATOR_VALUE",
            "$FACET_LEVEL_1$FACET_SEPARATOR_VALUE$century$FACET_SEPARATOR_VALUE$decade$FACET_SEPARATOR_VALUE"
        )
    }

    private val yearRegex = Regex("(\\d{4}).*")

    /**
     * Builds the hierarchical facet values for normalized date ranges.
     *
     * @param date Normalized date range (ISO-8601)
     *
     * @return The facet values to construct the hierarchy with century and decade.
     */
    fun buildFromNormalizedDateRange(date: String, language: String): List<String> {
        if (date.length <= 3) {
            log.error("Normalized date range has less than 4 characters: $date.")
            errorList.add("Normalized date range has less than 4 characters: $date.")
            return emptyList()
        }
        val from = date.substring(0, 4)
        val until = if (date.contains("/")) {
            val secondPart = date.split("/")[1]
            val matchResult = yearRegex.matchEntire(secondPart)
            matchResult.let { m ->
                if (m != null) {
                    m.groups[1]!!.value
                } else {
                    log.warn("Could not match year in normalized date range until value: $date.")
                    errorList.add("Could not match year in normalized date range until value: $date.")
                    ""
                }
            }
        } else {
            log.debug("Normalized date range does not have an until value: $date.")
            ""
        }

        return if (until.isEmpty()) {
            val century = getCentury(from, language)
            val decade = getDecade(from)
            listOf(
                "$FACET_LEVEL_0$FACET_SEPARATOR_VALUE$century$FACET_SEPARATOR_VALUE",
                "$FACET_LEVEL_1$FACET_SEPARATOR_VALUE$century$FACET_SEPARATOR_VALUE$decade$FACET_SEPARATOR_VALUE"
            )
        } else {
            val fromCentury = getCentury(from, language)
            val fromDecade = getDecade(from)

            val untilCentury = getCentury(until, language)
            val untilDecade = getDecade(until)

            if (fromCentury == untilCentury) {
                // from & until are in the same century.
                if (fromDecade == untilDecade) {
                    // from & until are in the same decade.
                    listOf(
                            "$FACET_LEVEL_0$FACET_SEPARATOR_VALUE$fromCentury$FACET_SEPARATOR_VALUE",
                            "$FACET_LEVEL_1$FACET_SEPARATOR_VALUE$fromCentury$FACET_SEPARATOR_VALUE$fromDecade$FACET_SEPARATOR_VALUE"
                    )
                } else {
                    // from & until are not in the same decade.
                    // as these are normalized values it should never happen that the until is before from.
                    val results = mutableListOf(
                            "$FACET_LEVEL_0$FACET_SEPARATOR_VALUE$fromCentury$FACET_SEPARATOR_VALUE",
                            "$FACET_LEVEL_1$FACET_SEPARATOR_VALUE$fromCentury$FACET_SEPARATOR_VALUE$fromDecade$FACET_SEPARATOR_VALUE"
                    )
                    var fromDecadeAsInt = fromDecade.substring(0, 4).toInt()
                    val untilDecadeAsInt = untilDecade.substring(0, 4).toInt()

                    while (fromDecadeAsInt < untilDecadeAsInt) {
                        fromDecadeAsInt += 10
                        results.add(
                                "$FACET_LEVEL_1$FACET_SEPARATOR_VALUE$fromCentury$FACET_SEPARATOR_VALUE${
                                    getDecade(
                                            fromDecadeAsInt
                                    )
                                }$FACET_SEPARATOR_VALUE"
                        )
                    }
                    results
                }
            } else {
                // from & until are not in the same century.
                val results = mutableListOf(
                        "$FACET_LEVEL_0$FACET_SEPARATOR_VALUE$fromCentury$FACET_SEPARATOR_VALUE"
                )
                // this number is one higher than it should be and is corrected in getCentury.
                var fromCenturyAsInt = fromCentury.substring(0, 2).toInt()
                val untilCenturyAsInt = untilCentury.substring(0, 2).toInt()

                // first adds all the centuries required
                while (fromCenturyAsInt < untilCenturyAsInt) {
                    results.add("$FACET_LEVEL_0$FACET_SEPARATOR_VALUE${getCentury(fromCenturyAsInt, language)}$FACET_SEPARATOR_VALUE")
                    fromCenturyAsInt += 1
                }
                // then add the original decade.
                results.add("$FACET_LEVEL_1$FACET_SEPARATOR_VALUE$fromCentury$FACET_SEPARATOR_VALUE$fromDecade$FACET_SEPARATOR_VALUE")
                var fromDecadeAsInt = fromDecade.substring(0, 4).toInt()
                val untilDecadeAsInt = untilDecade.substring(0, 4).toInt()

                // then add all the decades until the last decade is reached.
                while (fromDecadeAsInt < untilDecadeAsInt) {
                    fromDecadeAsInt += 10
                    results.add("$FACET_LEVEL_1$FACET_SEPARATOR_VALUE${getCentury(fromDecadeAsInt / 100, language)}" +
                            "$FACET_SEPARATOR_VALUE${getDecade(fromDecadeAsInt)}$FACET_SEPARATOR_VALUE")
                }
                results.sortedBy { v -> v.substring(2) }
            }
        }
    }

    private fun getCentury(year: String, language: String): String {
        return getCentury(year.substring(0, 2).toInt(), language)
    }

    private fun getCentury(century: Int, language: String): String {
        return "${century + 1}. ${centuryNames[language]}"
    }

    private fun getDecade(year: String): String {
        return getDecade(year.toInt())
    }

    private fun getDecade(year: Int): String {
        val reminder = year % 10
        val startYear = if (reminder == 0)
            year - 10
        else
            year - reminder
        return "${startYear + 1}-${startYear + 10}"
    }
}
