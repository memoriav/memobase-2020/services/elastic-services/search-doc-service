/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.helpers

import ch.memobase.Constants
import ch.memobase.Constants.FACET_LEVEL_0
import ch.memobase.Constants.FACET_LEVEL_1
import ch.memobase.Constants.FACET_SEPARATOR_VALUE
import ch.memobase.schemas.search.AgentWithRelationContainer
import ch.memobase.schemas.temp.AgentsMap
import ch.memobase.schemas.search.SimpleFacetContainer
import ch.memobase.rdf.FOAF
import ch.memobase.rdf.NS
import ch.memobase.rdf.RICO
import org.apache.logging.log4j.LogManager

@Suppress("UNCHECKED_CAST")
class AgentTransformer(private val functions: SharedSearchDocUtility) {
    private val log = LogManager.getLogger(this::class.java)
    private val personFacetValues = mutableSetOf<String>()
    private val personsNames = mutableSetOf<String>()
    val agentsMap = AgentsMap()

    private val person = NS.namespaceToPrefix(NS.rico) + ":" + RICO.Person.localName
    private val corporateBody = NS.namespaceToPrefix(NS.rico) + ":" + RICO.CorporateBody.localName
    private val agent = NS.namespaceToPrefix(NS.rico) + ":" + RICO.Agent.localName

    fun add(key: String, source: String, resource: Map<String, Any?>) {
        when (source) {
            Constants.RdaProperties.HAS_PRODUCER -> {
                when (val type = resource[Constants.RDF_TYPE]) {
                    person -> {
                        val values = getPersonFacetValues(key, resource)
                        personsNames.add(values.first)
                        personFacetValues.addAll(values.second)
                        agentsMap.persons.producer.add(
                            AgentWithRelationContainer(
                                functions.getTrilingualProperty(resource, RICO.name.localName),
                                null,
                                values.first,
                                values.second
                            )
                        )
                    }

                    corporateBody -> {
                        agentsMap.corporateBodies.producer.add(
                            AgentWithRelationContainer(
                                functions.getTrilingualProperty(resource, RICO.name.localName), null, null, emptyList()
                            )
                        )
                    }


                    agent -> {
                        agentsMap.agents.producer.add(
                            AgentWithRelationContainer(
                                functions.getTrilingualProperty(resource, RICO.name.localName), null, null, emptyList()
                            )
                        )
                    }

                    else -> throw InvalidInputException(
                        "Record $key contains a producer with an invalid rdf:type -> $type."
                    )
                }
            }

            RICO.hasPublisher.localName -> {
                when (val type = resource[Constants.RDF_TYPE]) {
                    person -> {
                        val values = getPersonFacetValues(key, resource)
                        personsNames.add(values.first)
                        personFacetValues.addAll(values.second)
                        agentsMap.persons.publisher.add(
                            AgentWithRelationContainer(
                                functions.getTrilingualProperty(resource, RICO.name.localName),
                                null,
                                values.first,
                                values.second
                            )
                        )
                    }

                    corporateBody -> {
                        agentsMap.corporateBodies.publisher.add(
                            AgentWithRelationContainer(
                                functions.getTrilingualProperty(resource, RICO.name.localName), null, null, emptyList()
                            )
                        )
                    }


                    agent -> {
                        agentsMap.agents.publisher.add(
                            AgentWithRelationContainer(
                                functions.getTrilingualProperty(resource, RICO.name.localName), null, null, emptyList()
                            )
                        )
                    }

                    else -> throw InvalidInputException(
                        "Record $key contains a publisher with an invalid rdf:type -> $type."
                    )
                }
            }

            RICO.hasOrHadSubject.localName -> {
                when (val type = resource[Constants.RDF_TYPE]) {
                    person -> {
                        val values = getPersonFacetValues(key, resource)
                        personsNames.add(values.first)
                        personFacetValues.addAll(values.second)
                        agentsMap.persons.subject.add(
                            AgentWithRelationContainer(
                                functions.getTrilingualProperty(resource, RICO.name.localName),
                                null,
                                values.first,
                                values.second
                            )
                        )
                    }

                    corporateBody -> {
                        agentsMap.corporateBodies.subject.add(
                            AgentWithRelationContainer(
                                functions.getTrilingualProperty(resource, RICO.name.localName), null, null, emptyList()
                            )
                        )
                    }


                    agent -> {
                        agentsMap.agents.subject.add(
                            AgentWithRelationContainer(
                                functions.getTrilingualProperty(resource, RICO.name.localName), null, null, emptyList()
                            )
                        )
                    }

                    else -> {
                        // this is a log entry because there are some records with different types that are
                        // not relevant for the search
                        log.warn("Ignored subject for agent transform because it is not of an agent type: $type.")
                    }
                }
            }

            else -> {
                throw Exception("Parameter source $source is not a valid value!")
            }
        }
    }

    fun addCreationRelation(key: String, resource: Map<String, Any?>) {
        when (val ricoType = resource[RICO.type.localName]) {
            RICO.Types.CreationRelation.creator -> {
                mapCreationRelationData(
                    key,
                    resource,
                    agentsMap.persons.creator,
                    agentsMap.corporateBodies.creator,
                    agentsMap.agents.creator
                )
            }

            RICO.Types.CreationRelation.contributor -> {
                mapCreationRelationData(
                    key,
                    resource,
                    agentsMap.persons.contributor,
                    agentsMap.corporateBodies.contributor,
                    agentsMap.agents.contributor
                )
            }

            RICO.Types.CreationRelation.exifArtist -> {
                // Do nothing with those currently.
            }

            null -> {
                throw InvalidInputException("Creation relation without a type encountered in record $key!")
            }

            else -> {
                throw InvalidInputException("Creation relation with an unexpected type encountered $ricoType!")
            }
        }
    }


    fun getPersonFacetContainer(): SimpleFacetContainer {
        return SimpleFacetContainer(
            personFacetValues.toList().sortedBy { v -> v.substring(2) }, personsNames.toList().sorted()
        )
    }

    private fun mapCreationRelationData(
        key: String,
        resource: Map<String, Any?>,
        persons: MutableList<AgentWithRelationContainer>,
        corporateBodies: MutableList<AgentWithRelationContainer>,
        agents: MutableList<AgentWithRelationContainer>,
    ) {
        when (val target = resource[RICO.creationRelationHasTarget.localName]) {
            is Map<*, *> -> {
                when (target[Constants.RDF_TYPE]) {
                    person -> {
                        persons.add(
                            mapCreationRelationPersonData(key, target as Map<String, Any?>, resource)
                        )
                    }

                    corporateBody -> {
                        corporateBodies.add(
                            mapCreationRelationAgentData(target as Map<String, Any?>, resource)
                        )
                    }

                    agent -> {
                        agents.add(
                            mapCreationRelationAgentData(target as Map<String, Any?>, resource)
                        )
                    }
                }
            }

            else -> throw InvalidInputException("Creation relation does not have a valid target for record $key.")
        }
    }

    private fun mapCreationRelationPersonData(
        key: String, target: Map<String, Any?>, resource: Map<String, Any?>
    ): AgentWithRelationContainer {
        val values = getPersonFacetValues(key, target)
        personsNames.add(values.first)
        personFacetValues.addAll(values.second)
        return AgentWithRelationContainer(
            functions.getTrilingualProperty(target, RICO.name.localName),
            functions.getTrilingualProperty(resource, RICO.name.localName),
            values.first,
            values.second
        )
    }

    private fun mapCreationRelationAgentData(
        target: Map<String, Any?>, resource: Map<String, Any?>
    ): AgentWithRelationContainer {
        return AgentWithRelationContainer(
            functions.getTrilingualProperty(
                target, RICO.name.localName
            ), functions.getTrilingualProperty(resource, RICO.name.localName), null, emptyList()
        )
    }

    private fun getPersonFacetValues(key: String, map: Map<String, Any?>): Pair<String, List<String>> {
        val name = when {
            map.containsKey(FOAF.lastName.localName) -> {
                val lastName = try {
                    map[FOAF.lastName.localName] as String
                } catch (ex: ClassCastException) {
                    throw InvalidInputException("The last name of a person is not a string: ${map[FOAF.lastName.localName]}. Attempted to cast this value to a string and failed.")
                }

                Pair(lastName, lastName + map[FOAF.firstName.localName].let { if (it is String) ", $it" else "" })
            }

            map.containsKey(RICO.name.localName) -> {
                val name = try {
                    map[RICO.name.localName] as String
                } catch (ex: ClassCastException) {
                    throw InvalidInputException("The full name of a person is not a string: ${map[RICO.name.localName]}. Attempted to cast this value to a string and failed.")
                }
                Pair(name, name)
            }

            else -> {
                throw InvalidInputException("No name defined for person in record $key for person $map.")
            }
        }
        val capitalLetter = Utility.getCapitalLetter(name.first)

        return Pair(
            name.second, listOf(
                "${FACET_LEVEL_0}${FACET_SEPARATOR_VALUE}$capitalLetter${FACET_SEPARATOR_VALUE}",
                "${FACET_LEVEL_1}${FACET_SEPARATOR_VALUE}$capitalLetter${FACET_SEPARATOR_VALUE}${name.second}${FACET_SEPARATOR_VALUE}"
            )
        )
    }
}