/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.helpers

import java.lang.NumberFormatException

object AspectRatio {
    fun asFraction(a: String, b: String): String {
        val editedA = removeDot(a)
        val editedB = removeDot(b)

        val aLong = try {
            editedA.toLong()
        } catch (ex: NumberFormatException) {
            return ""
        }
        val bLong = try {
            editedB.toLong()
        } catch (ex: NumberFormatException) {
            return ""
        }
        val gcd = gcd(aLong, bLong)
        return (aLong / gcd).toString() + "/" + (bLong / gcd).toString()
    }

    private fun gcd(a: Long, b: Long): Long {
        return if (b == 0L) a else gcd(b, a % b)
    }

    private fun removeDot(value: String): String {
        return if (value.contains(".")) {
            value.substringBefore(".")
        } else {
            value
        }
    }
}