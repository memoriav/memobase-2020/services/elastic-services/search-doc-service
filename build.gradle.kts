plugins {
    application
    distribution
    jacoco
    kotlin("jvm") version "1.9.24"
    id("io.freefair.git-version") version "6.2.0"
    id("org.jetbrains.kotlin.plugin.serialization") version "1.9.22"
    // id("org.jlleitschuh.gradle.ktlint") version "12.1.1"
    id("org.jetbrains.dokka") version "1.9.20"
}

group = "ch.memobase"

application {
    mainClass.set("ch.memobase.App")
    tasks.withType<Tar>().configureEach {
        archiveFileName = "app.tar"
    }
}

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(21)
    }
}

repositories {
    mavenCentral()
    maven {
        setUrl("https://gitlab.switch.ch/api/v4/projects/1324/packages/maven")
    }
}

dependencies {
    // https://gitlab.switch.ch/memoriav/memobase-2020/libraries/service-utilities/-/tags
    implementation("ch.memobase:memobase-service-utilities:4.14.0")

    // https://central.sonatype.com/artifact/org.apache.jena/apache-jena
    implementation("org.apache.jena:apache-jena:5.1.0")

    // https://central.sonatype.com/artifact/org.jetbrains.kotlinx/kotlinx-serialization-json
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.3")

    // Logging Framework
    // https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-core
    implementation("org.apache.logging.log4j:log4j-api:2.23.1")
    implementation("org.apache.logging.log4j:log4j-core:2.23.1")
    // https://central.sonatype.com/artifact/org.apache.logging.log4j/log4j-slf4j18-impl
    implementation("org.apache.logging.log4j:log4j-slf4j18-impl:2.18.0")

    // Kafka Imports
    // https://central.sonatype.com/artifact/org.apache.kafka/kafka-clients
    implementation("org.apache.kafka:kafka-streams:3.8.0")

    // JSON Parser
    implementation("com.fasterxml.jackson.core:jackson-databind:2.17.1")
    implementation("com.fasterxml.jackson.core:jackson-core:2.17.1")
    implementation("com.fasterxml.jackson.core:jackson-annotations:2.17.1")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.17.1")

    // JSON Parser
    implementation("com.beust:klaxon:5.6")
    // CSV Reader
    implementation("com.github.doyaaaaaken:kotlin-csv-jvm:1.6.0")

    // https://central.sonatype.com/artifact/org.junit.jupiter/junit-jupiter
    testImplementation("org.junit.jupiter:junit-jupiter:5.11.0")
    // https://central.sonatype.com/artifact/org.junit.jupiter/junit-jupiter-engine
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.11.0")
    // https://central.sonatype.com/artifact/org.junit.jupiter/junit-jupiter-api
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.11.0")
    // https://central.sonatype.com/artifact/org.assertj/assertj-core
    testImplementation("org.assertj:assertj-core:3.25.3")

    // https://mvnrepository.com/artifact/org.apache.kafka/kafka-streams-test-utils
    testImplementation("org.apache.kafka:kafka-streams-test-utils:3.7.0")
}

configurations {
    all {
        exclude(group = "org.slf4j", module = "slf4j-log4j12")
    }
}

tasks.named<Test>("test") {
    useJUnitPlatform()

    testLogging {
        setEvents(mutableListOf("passed", "skipped", "failed"))
    }
}

tasks.jacocoTestReport {
    dependsOn(tasks.test)
    reports {
        csv.required = true
    }
}
